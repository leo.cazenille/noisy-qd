#!/bin/bash


declare -a configs


###### BIPEDAL ######



###### RASTRIGIN ######

#configs+=("conf/rastrigin2vs6-noiseless-MutPolyBounded0.1x0.9-Grid64x64.yaml 1")
#
##configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-AdaptiveSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 1")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 1")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FBDR1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
##configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FixedSampling10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FixedSampling1-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FixedSampling5-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-TimeBasedSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#
##configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-AdaptiveSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FBDR1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
##configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FixedSampling10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FixedSampling1-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FixedSampling5-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-TimeBasedSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#
##configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-AdaptiveSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FBDR1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
##configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FixedSampling10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FixedSampling1-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FixedSampling5-MutPolyBounded0.1x0.9-Grid64x64.yaml 3")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 1")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-TimeBasedSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 1")
#
##configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-AdaptiveSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 1")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9-Grid64x64.yaml 5")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FBDR1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 1")
##configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FixedSampling10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FixedSampling1-MutPolyBounded0.1x0.9-Grid64x64.yaml 1")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FixedSampling5-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 6")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-TimeBasedSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 4")


#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FS1xFS5-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FS1xAS1x10-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FS1xCBAS1x10x0.05-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FS1xAS1x10-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 1")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FS1xCBAS1x10x0.05-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 1")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FS1xFS5-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 1")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FS1xAS1x10-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FS1xCBAS1x10x0.05-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FS1xFS5-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FS1xAS1x10-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FS1xCBAS1x10x0.05-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FS1xFS5-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 2")

#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FS1xFS5-MutPolyBounded0.1x0.9-0.99-Grid64x64.yaml 2")

#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-AMS_TBSxFBDRxAS1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-AMS_TBSxFBDRxAS1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-AMS_TBSxFBDRxAS1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-AMS_TBSxFBDRxAS1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")

#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-AMS_TBSxDSSxAS1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-AMS_TBSxDSSxAS1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-AMS_TBSxDSSxAS1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-AMS_TBSxDSSxAS1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")

## CBAS
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-ConfidenceBasedAdaptiveSampling1x10x0.10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-ConfidenceBasedAdaptiveSampling1x10x0.15-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-ConfidenceBasedAdaptiveSampling1x10x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")

## SEDR
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-SEDR2x10x0.0x0.30x0.0x0.10-MutPolyBounded0.1x0.9-Grid64x64.yaml 8")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-SEDR2x10x0.0x0.30x0.0x0.30-MutPolyBounded0.1x0.9-Grid64x64.yaml 8")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-SEDR2x10x0.0x0.40x0.0x0.40-MutPolyBounded0.1x0.9-Grid64x64.yaml 8")



#
## Uniform
#configs+=("conf/rastrigin2vs6-fitness_features_uniform0.01x0.01-AdaptiveSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform0.01x0.01-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform0.01x0.01-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform0.01x0.01-FBDR1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform0.01x0.01-FixedSampling1-MutPolyBounded0.1x0.9-Grid64x64.yaml 8")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform0.01x0.01-FixedSampling10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform0.01x0.01-FixedSampling5-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform0.01x0.01-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform0.01x0.01-TimeBasedSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#
#configs+=("conf/rastrigin2vs6-fitness_features_uniform1.0x1.0-AdaptiveSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform1.0x1.0-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform1.0x1.0-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform1.0x1.0-FBDR1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform1.0x1.0-FixedSampling1-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform1.0x1.0-FixedSampling10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform1.0x1.0-FixedSampling5-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform1.0x1.0-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_uniform1.0x1.0-TimeBasedSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2")
#



#
#configs+=("conf/rastrigin2vs6-noiseless-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-AdaptiveSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FBDR1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FixedSampling10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FixedSampling1-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FixedSampling5-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-TimeBasedSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-AdaptiveSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FBDR1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FixedSampling10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FixedSampling1-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FixedSampling5-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-TimeBasedSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-AdaptiveSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FBDR1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FixedSampling10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FixedSampling1-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-FixedSampling5-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian0.01-TimeBasedSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-AdaptiveSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FBDR1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FixedSampling10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FixedSampling1-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-FixedSampling5-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")
#configs+=("conf/rastrigin2vs6-fitness_only_gaussian1.0-TimeBasedSampling1x10-MutPolyBounded0.1x0.9x1.0-Grid64x64.yaml 2")


printf -v allParams "%s\n" "${configs[@]}"
#echo $allParams | xargs --max-procs=$(nproc --all) -n 2 ./runDockerExpe.sh
echo $allParams | xargs --max-procs=12 -n 2 ./runDockerExpe.sh




#./runDockerExpe.sh conf/rastrigin2vs6-noiseless-MutPolyBounded0.1x0.9-Grid64x64.yaml 1 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-AdaptiveSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FBDR1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FixedSampling10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FixedSampling1-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-FixedSampling5-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian0.01x0.01-TimeBasedSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#
#wait 
#
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-AdaptiveSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FBDR1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FixedSampling10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FixedSampling1-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-FixedSampling5-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_features_gaussian1.0x1.0-TimeBasedSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#
#wait
#
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian0.01-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian0.01-AdaptiveSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian0.01-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian0.01-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian0.01-FBDR1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian0.01-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian0.01-FixedSampling10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian0.01-FixedSampling1-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian0.01-FixedSampling5-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian0.01-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian0.01-TimeBasedSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#
#wait 
#
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian1.0-ActivityStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian1.0-AdaptiveSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian1.0-ConfidenceBasedAdaptiveSampling1x10x0.05-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian1.0-DominationStrengthSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian1.0-FBDR1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian1.0-FBSEDR2x10x0.20x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian1.0-FixedSampling10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian1.0-FixedSampling1-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian1.0-FixedSampling5-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian1.0-SEDR2x10x0.0x0.20x0.0x0.20-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#./runDockerExpe.sh conf/rastrigin2vs6-fitness_only_gaussian1.0-TimeBasedSampling1x10-MutPolyBounded0.1x0.9-Grid64x64.yaml 2 &
#
#wait


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
