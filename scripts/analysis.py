#!/usr/bin/env python3

"""Analyse the results of several experiments, compute their ERT, and output a PDF table."""


from fitness import *
from stats import *
from plots import *
from algos import *

from qdpy import algorithms, containers, benchmarks, plots

import numpy as np
import warnings
import os
import random
import yaml
import datetime
import pathlib
import sys
import glob
import gc

from pylatex import Document, Section, Subsection, Tabular, MultiColumn, MultiRow, MiniPage
import pylatex.utils


#targets_global_rel = 0.1, 0.3, 0.5, 0.7, 0.90, 0.95, 0.99, 0.999, 1.0
#targets_noiseless_ratio = 0.1, 0.3, 0.5, 0.7, 0.90, 0.95, 0.99, 0.999, 1.0
#targets_fitness = 0.1, 0.3, 0.5, 0.7, 0.90, 0.95, 0.99, 0.999, 1.0

#max_budget_evals = 1000000
max_budget_evals = 1e2 * 64*64 * 6
#targets = (0.1, 0.3, 0.5, 0.7, 0.90, 0.95, 0.99, 0.999, 0.9999)
#targets = (0.1, 0.3, 0.5, 0.7, 0.90, 0.95, 0.99, 0.995, 0.999)
targets = (0.1, 0.3, 0.5, 0.6, 0.7, 0.8, 0.90, 0.95, 0.99)

fitness_domain = (0., benchmarks.artificial_landscapes.rastrigin([4.5]*2, 10.)[0])  # XXX



def compute_stats(all_expe_data):
    n = len(all_expe_data)
    res = {}

    # Declare stats arrays
    global_rel_succ_rate = np.zeros(len(targets))
    global_rel_rts = np.zeros(len(targets))
    noiseless_ratio_succ_rate = np.zeros(len(targets))
    noiseless_ratio_rts = np.zeros(len(targets))
    fit_ratio_succ_rate = np.zeros(len(targets))
    fit_ratio_rts = np.zeros(len(targets))

    # Analyze all data
    for tmp in range(n):
        ##d = all_expe_data.pop()
        ##cont = d['container']
        #cont = all_expe_data.pop()

        #evals = cont.sampling_decorator_nb_evaluations
        #gl = cont.global_reliability
        #noiseless_ratio = [x / cont.size for x in cont.nb_noiseless]
        #fit = np.array([f[0] if f is not None else fitness_domain[1] for f in cont.best_fitness_noiseless])

        d = all_expe_data.pop()
        evals = d[0]
        gl = d[1]
        noiseless_ratio = [x / d[2] for x in d[3]]
        #fit = np.array([f[0] if f is not None else fitness_domain[1] for f in d[4]])
        fit = d[4]


        fit_ratio = 1. - np.abs(fit - fitness_domain[0]) / (fitness_domain[1] - fitness_domain[0])
        current_gl_target_idx = 0
        current_noiseless_target_idx = 0
        current_fit_target_idx = 0
        for i, e in enumerate(evals):
            if e >= max_budget_evals:
                break
            if current_gl_target_idx < len(targets) and gl[i] > targets[current_gl_target_idx]:
                global_rel_succ_rate[current_gl_target_idx] += 1
                global_rel_rts[current_gl_target_idx] += e
                current_gl_target_idx += 1
            if current_noiseless_target_idx < len(targets) and noiseless_ratio[i] > targets[current_noiseless_target_idx]:
                noiseless_ratio_succ_rate[current_noiseless_target_idx] += 1
                noiseless_ratio_rts[current_noiseless_target_idx] += e
                current_noiseless_target_idx += 1
            if current_fit_target_idx < len(targets) and fit_ratio[i] > targets[current_fit_target_idx]:
                fit_ratio_succ_rate[current_fit_target_idx] += 1
                fit_ratio_rts[current_fit_target_idx] += e
                current_fit_target_idx += 1

    # Compute stats
    #global_rel_rts /= global_rel_succ_rate
    global_rel_rts = np.divide(global_rel_rts, global_rel_succ_rate, out=np.zeros_like(global_rel_rts), where=global_rel_succ_rate!=0)
    global_rel_succ_rate /= n
    global_rel_ert = global_rel_succ_rate * global_rel_rts + (1. - global_rel_succ_rate) * max_budget_evals
    #noiseless_ratio_rts /= noiseless_ratio_succ_rate
    noiseless_ratio_rts = np.divide(noiseless_ratio_rts, noiseless_ratio_succ_rate, out=np.zeros_like(noiseless_ratio_rts), where=noiseless_ratio_succ_rate!=0)
    noiseless_ratio_succ_rate /= n
    noiseless_ratio_ert = noiseless_ratio_succ_rate * noiseless_ratio_rts + (1. - noiseless_ratio_succ_rate) * max_budget_evals
    #fit_ratio_rts /= fit_ratio_succ_rate
    fit_ratio_rts = np.divide(fit_ratio_rts, fit_ratio_succ_rate, out=np.zeros_like(fit_ratio_rts), where=fit_ratio_succ_rate!=0)
    fit_ratio_succ_rate /= n
    fit_ratio_ert = fit_ratio_succ_rate * fit_ratio_rts + (1. - fit_ratio_succ_rate) * max_budget_evals
    res['global_rel_succ_rate'] = global_rel_succ_rate
    res['global_rel_ert'] = global_rel_ert
    res['nb_noiseless_ratio_succ_rate'] = noiseless_ratio_succ_rate
    res['nb_noiseless_ratio_ert'] = noiseless_ratio_ert
    res['bestfit_ratio_succ_rate'] = fit_ratio_succ_rate
    res['bestfit_ratio_ert'] = fit_ratio_ert
    return res



def generate_latex_table(stats, output, splitIndexes = []):
    #geometry_options = {"tmargin": "0cm", "lmargin": "0cm"}
    doc = Document(output, documentclass="standalone", page_numbers=False)
    with doc.create(Tabular('|c|c|' + 'c|' * len(targets))) as table:
        table.add_hline()
        table.add_row(("Alg.", "Stat.") + tuple((f"ERT Target {i}" for i in targets)))
        table.add_hline()

        # Retrieve stats
        labels = []
        global_rel_ert = np.zeros((len(stats), len(targets)))
        global_rel_succ_rate = np.zeros((len(stats), len(targets)))
        nb_noiseless_ratio_ert = np.zeros((len(stats), len(targets)))
        nb_noiseless_ratio_succ_rate = np.zeros((len(stats), len(targets)))
        bestfit_ratio_ert = np.zeros((len(stats), len(targets)))
        bestfit_ratio_succ_rate = np.zeros((len(stats), len(targets)))
        for i,k in enumerate(stats.keys()):
            v = stats[k]
            labels.append(k)
            global_rel_ert[i,:] = v['global_rel_ert']
            global_rel_succ_rate[i,:] = v['global_rel_succ_rate']
            nb_noiseless_ratio_ert[i,:] = v['nb_noiseless_ratio_ert']
            nb_noiseless_ratio_succ_rate[i,:] = v['nb_noiseless_ratio_succ_rate']
            bestfit_ratio_ert[i,:] = v['bestfit_ratio_ert']
            bestfit_ratio_succ_rate[i,:] = v['bestfit_ratio_succ_rate']

        # Find the indexes of the best ERTs -- ignore experiment 0, which is assumed to be the reference case (noiseless)
        idxbest_global_rel_ert = np.nanargmin(global_rel_ert[1:], axis=0) + 1
        idxbest_nb_noiseless_ratio_ert = np.nanargmin(nb_noiseless_ratio_ert[1:], axis=0) + 1
        idxbest_bestfit_ratio_ert = np.nanargmin(bestfit_ratio_ert[1:], axis=0) + 1
#        print("DEBUG: global_rel_ert: ", global_rel_ert)
#        print("DEBUG: nb_noiseless_ratio_ert: ", nb_noiseless_ratio_ert)
#        print("DEBUG: bestfit_ratio_ert: ", bestfit_ratio_ert)
#        print("DEBUG: idxbests: ", idxbest_global_rel_ert, idxbest_nb_noiseless_ratio_ert, idxbest_bestfit_ratio_ert)

        def entries(i, ert, succ_rate, bold):
            s_ert = "-"
            if succ_rate[i]:
                s_ert = f"{ert[i]:.4g}"
                if bold:
                    s_ert = pylatex.utils.bold(s_ert)
            s_succ_rate = f" ({succ_rate[i]:.1f})"
            return s_ert + s_succ_rate

        # Create the table
        for j in range(len(stats)):
            if j in splitIndexes:
                table.add_hline()
            s_gr = tuple( entries(i, global_rel_ert[j], global_rel_succ_rate[j], idxbest_global_rel_ert[i]==j) for i in range(len(targets)) )
            table.add_row((MultiRow(3, data=labels[j]), 'Global rel.') + s_gr, escape=False)
            s_full = tuple( entries(i, nb_noiseless_ratio_ert[j], nb_noiseless_ratio_succ_rate[j], idxbest_nb_noiseless_ratio_ert[i]==j) for i in range(len(targets)) )
            table.add_row(("", 'Cont. fullness') + s_full, escape=False)
            s_bestfit = tuple( entries(i, bestfit_ratio_ert[j], bestfit_ratio_succ_rate[j], idxbest_bestfit_ratio_ert[i]==j) for i in range(len(targets)) )
            table.add_row(("", 'Best Fit. ratio') + s_bestfit, escape=False)
            table.add_hline()

        table.add_hline()
    doc.generate_pdf(output, clean_tex=False)
    #doc.generate_pdf(output)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputDirs', type=str, default='', help = "Directories of input data, separated by a ','")
    parser.add_argument('-n', '--expeNames', type=str, default='', help = "Names of all experiments, separated by a ','")
    parser.add_argument('-D', '--baseDir', type=str, default='results/', help = "Base directory of all data")
    parser.add_argument('-o', '--outputPrefix', type=str, default="results", help = "Prefix name of the output files")
    parser.add_argument('--nbRuns', type=int, default=15, help = "Number of runs to take into account")
    parser.add_argument('--splitIndexes', type=str, default="", help = "Indexes of splits in the table")
    args = parser.parse_args()

    input_dirs = args.inputDirs.split(",")
    baseDir = args.baseDir if len(args.baseDir) > 0 else "./"
    input_dirs = [os.path.expanduser(os.path.join(baseDir, d)) for d in input_dirs]
    expe_names = args.expeNames.split(",")
    splitIndexes = args.splitIndexes.split(",")

    #global_rel_succ_rate = np.zeros((len(expe_names), len(targets)))
    #global_rel_ert = np.zeros((len(expe_names), len(targets)))
    stats = {}

    for expe_name, input_dir in zip(expe_names, input_dirs):
        print(f"Computing stats for expe '{expe_name}'...")
        expe_files = list(glob.glob(os.path.join(input_dir, "final*.p"), recursive=False))
        expe_files = expe_files[:args.nbRuns]
        all_expe_data = []
        for filename in expe_files:
            #print("DEBUG:", filename)
            with open(filename, "rb") as f:
                data = pickle.load(f)
            cont = data['container']
            best_fitness_noiseless = np.array([f[0] if f is not None and hasattr(f, '__getitem__') else fitness_domain[1] for f in cont.best_fitness_noiseless])
            d = (np.array(cont.sampling_decorator_nb_evaluations), np.array(cont.global_reliability), np.array(cont.size), np.array(cont.nb_noiseless), best_fitness_noiseless)
            #all_expe_data.append(data['container'])
            all_expe_data.append(d)
            gc.collect()
        stats[expe_name] = compute_stats(all_expe_data)
        gc.collect()

    with open(args.outputPrefix + ".p", "wb") as f:
        pickle.dump(stats, f)

    generate_latex_table(stats, args.outputPrefix, splitIndexes)


# MODELINE "{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
