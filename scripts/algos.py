#!/usr/bin/env python3

""" TODO """


import math
#import time
#from timeit import default_timer as timer
#from inspect import signature
from typing import Optional, Tuple, List, Iterable, Iterator, Any, TypeVar, Generic, Union, Sequence, MutableSet, MutableSequence, Type, Callable, Generator, Mapping, MutableMapping, overload
from typing_extensions import runtime, Protocol
#import traceback
import warnings
import numpy as np
import copy
import os
import random
from timeit import default_timer as timer
import scipy.stats


from qdpy.utils import *
from qdpy.base import *
from qdpy.containers import *
from qdpy import tools
from qdpy.algorithms import *

import functools


@runtime
class SamplingLike(Protocol):
    """TODO"""
    def sample(self, collection: Sequence[Any]) -> None: ...


class ExplicitAveragingSampling(SamplingLike):
    """TODO"""
    def sample(self, collection: Sequence[Any]) -> None:
        if len(collection) > 0:
            #print("DEBUG ExplicitAveragingSampling:", collection, np.mean(np.array(collection), axis=0))
            return np.mean(np.array(collection), axis=0)
        else:
            return ()


class SampledFitness(Fitness):
    """TODO"""
    sampling: SamplingLike
    #nb_samples: int
    _samples: Sequence[FitnessValuesLike]

#    def __new__(cls, values: FitnessValuesLike=(), weights: Optional[FitnessValuesLike]=None):
#        return super(Fitness, cls).__new__(cls)

    def __init__(self, sampling: SamplingLike = ExplicitAveragingSampling(), values: FitnessValuesLike=(), weights: Optional[FitnessValuesLike]=None) -> None:
        self.sampling = sampling
        self._samples = []
        super().__init__(values, weights)
        self._compute_values()

    def reset(self):
        self._samples = []
        self.wvalues = (np.nan,) * len(self.wvalues)

    def _compute_values(self) -> None:
        values = tuple(self.sampling.sample(self._samples))
        #self.wvalues = values
        self.wvalues = tuple(map(mul, values, self.weights))

    @property
    def values(self) -> FitnessValuesLike:
        #print("values:", self.wvalues, self.weights, tuple(map(truediv, self.wvalues, self.weights)))
        return tuple(map(truediv, self.wvalues, self.weights))

    @values.setter
    def values(self, values: FitnessValuesLike) -> None:
        #self.add_sample(tuple(map(mul, values, self.weights)))
        self.add_sample(values)
        #try:
        #    self.wvalues = tuple(map(mul, values, self.weights))
        #except TypeError:
        #    raise ValueError("Invalid ``value`` parameter.")

#    @values.deleter
#    def values(self) -> None:
#        self.wvalues = ()


    @property
    def samples(self) -> Sequence[FeaturesLike]:
        return tuple(self._samples)

    @property
    def nb_samples(self) -> int:
        return len(self._samples)

    def add_sample(self, sample) -> None:
        if sample != ():
            self._samples.append(sample)
            self._compute_values()
        #print("DEBUG add_sample:", sample, self._samples)

    def merge(self, other) -> None:
        self._samples += [x for x in other._samples if x != ()]
        self._compute_values()
        #print("DEBUG merge:", self._samples)



class SampledFeatures(list, FeaturesLike):
    """TODO"""
    sampling: SamplingLike
    #_nb_samples: int
    _values: FeaturesLike
    _samples: Sequence[FeaturesLike]
    #_samples_changed: bool

    def __init__(self, sampling: SamplingLike = ExplicitAveragingSampling(), *args, **kwargs) -> None:
        self.sampling = sampling
        self._samples = []
        #self._samples_changed = True
        super().__init__(*args, **kwargs)
        self._compute_values()

    def reset(self):
        self._samples = []
        self._values = (np.nan,) * len(self._values)

    def _compute_values(self) -> None:
        #if self._samples_changed:
        #print("DEBUG _compute_values:", self._samples, self.sampling.sample(self._samples))
        self._values = tuple(self.sampling.sample(self._samples))

    @property
    def values(self) -> FeaturesLike:
        #self._compute_values()
        return self._values

    @values.setter
    def values(self, values: FeaturesLike) -> None:
        self.add_sample(values)

    @property
    def samples(self) -> Sequence[FeaturesLike]:
        return tuple(self._samples)

    @property
    def nb_samples(self) -> int:
        return len(self._samples)

    def add_sample(self, sample) -> None:
        #self._samples_changed = True
        self._samples.append(sample)
        self._compute_values()

    def merge(self, other) -> None:
        self._samples += other._samples
        self._compute_values()

    def __getitem__(self, key):
        return self._values[key]

    def __setitem__(self, idx, value):
        if idx == slice(None, None, None):
            self.add_sample(value)
        else:
            self._values[idx] = value

    def __iter__(self) -> Iterator:
        return iter(self._values)

    def __len__(self) -> int:
        return len(self._values)

    def __hash__(self) -> int:
        return hash(self._values)



class SampledIndividual(Individual):
    def __init__(self, iterable: Optional[Iterable] = None,
            name: Optional[str] = None,
            fitness: FitnessLike = SampledFitness(), features: FeaturesLike = SampledFeatures()) -> None:
        super().__init__(iterable, name, fitness, features)
        self.noiseless_fitness = None
        self.noiseless_features = None
        self.optim_flags = {}

    def reset(self):
        self.fitness.reset()
        # XXX HACK
        if hasattr(self.features, 'reset'): 
            self.features.reset()
        else:
            self.features = ()

        self.noiseless_fitness = None
        self.noiseless_features = None

    def merge(self, other):
        self.fitness.merge(other.fitness)
        self.features.merge(other.features)
        self.elapsed += other.elapsed
        if self.noiseless_fitness is None:
            self.noiseless_fitness = other.noiseless_fitness
        if self.noiseless_features is None:
            self.noiseless_features = other.noiseless_features

    @property
    def nb_samples(self) -> int:
        return self.fitness.nb_samples

    def to_ind(self, use_noiseless_fitness=True, use_noiseless_features=True) -> Individual:
        fitness = self.noiseless_fitness if use_noiseless_fitness else self.fitness
        features = self.noiseless_features if use_noiseless_features else self.features
        res = Individual(self, self.name, Fitness(fitness), features)
        return res


def gen_sampled_individuals():
    while(True):
        yield SampledIndividual()


#
## TODO : recode class Individual to store several fitness/features values ???
##  --> must be compatible with old code !!
##  --> temporary solution: store nb_repeats in Individual
##       --> tell function recompute averaged value
##  --> create a subclass of Fitness to handle repeats: ExplicitAveragingFitness ?
##       --> refactor qdpy to handle more complex fitnesses
##  --> create a subclass of Features to handle repeats: ExplicitAveragingFeatures ?
##       --> refactor qdpy to handle more complex features
#class NoisyIndividual(Individual):
#    nb_repeats: int
#
#    def __init__(self, iterable: Optional[Iterable] = None,
#            name: Optional[str] = None,
#            fitness: Optional[FitnessLike] = None, features: FeaturesLike = ()) -> None:
#        super().__init__(iterable, name, fitness, features)
#        if fitness is not None:
#            self.nb_repeats = 1
#        else:
#            self.nb_repeats = 0
#
#    def __eq__(self, other):
#        return (self.__class__ == other.__class__ and tuple(self) == tuple(other))
#
#    def merge(self, other):
#        tmp_fitness = np.array(self.fitness) * self.nb_repeats
#        tmp_features = np.array(self.features) * self.nb_repeats
#        other_fitness = np.array(other.fitness) * other.nb_repeats
#        other_features = np.array(other.features) * other.nb_repeats
#        new_fitness = (tmp_fitness + other_fitness) / (self.nb_repeats + other.nb_repeats)
#        new_features = (tmp_features + other_features) / (self.nb_repeats + other.nb_repeats)
#        #print("merge:", self.nb_repeats, other.nb_repeats)
#        self.fitness.setValues(tuple(new_fitness))
#        self.features[:] = list(new_features)
#        self.nb_repeats += other.nb_repeats
#        self.elapsed += other.elapsed
#
#def gen_noisy_individuals():
#    while(True):
#        yield NoisyIndividual()
#


# STRUCTURE:
#--> ask
#    --> si inds_to_ask vide:
#        --> return decorated.ask
#    --> else
#        --> return inds_to_ask.pop()
#--> while True:
#    --> calcul de la fitness (1 repeat)
#    --> tell_and_return_replaced_elite + add dans le container. Renvoie une exception s'il n'y a pas assez de repeats
#        --> si ca a remplacé un elite:
#            --> ajoute elite dans inds_to_ask
#    --> if exception "NotEnoughRepeats"
#        --> continue
#    --> else
#        --> break

# STRUCTURE 2:
#   - staging system : i.e. individuals that can not yet be added to the container or compared with elites of the container, because they do not have enough retrials yet
#   - ask
#       --> si inds_to_ask vide:
#           --> return decorated.ask
#       --> else
#           --> return inds_to_ask.pop()
#   --> calcul de la fitness (1 repeat)
#   --> tell.
#       --> si ind est dans le staging_list
#           --> merge staging_list_ind avec ind, et replace ind par staging_list_ind
#       --> si ind peut se mettre dans un bin vide, ajoute
#       --> sinon si ind a une fitness plus petite ou egale que l'elite, n'ajoute pas, rajoute elite dans inds_to_ask
#       --> sinon (ind a une fitness strictement plus grande que l'élite)
#           --> si ind a plus (ou autant) de repeats que elite, ajoute
#           --> sinon (ind a strictement un plus petit nb de repeats que elite)
#               --> si ind est deja dans inds_to_ask, ne fait rien et termine
#               --> sinon, rajoute k * ind dans inds_to_ask
#       --> si on a ajouté et que ind était dans la staging_list, retire ind de la staging_list





class SamplingDecorator(QDAlgorithm):
    """TODO"""

    def __init__(self, base_alg: QDAlgorithm, **kwargs):
        self.base_alg = base_alg
        self.base_alg_batch_start = 0.
        self.inds_to_ask = []
        self.staging_inds = []

        kwargs['batch_size'] = kwargs.get('batch_size') or min(base_alg.batch_size // 10, 10) # XXX HACK
        kwargs['dimension'] = kwargs.get('dimension') or base_alg.dimension
        kwargs['optimisation_task'] = kwargs.get('optimisation_task') or base_alg.optimisation_task
        kwargs['ind_domain'] = kwargs.get('ind_domain') or base_alg.ind_domain
        kwargs['name'] = kwargs.get('name') or base_alg.name
        kwargs['base_ind_gen'] = kwargs.get('base_ind_gen') or base_alg._base_ind_gen
        kwargs['budget'] = kwargs.get('budget') or base_alg.budget
        super().__init__(base_alg.container, **kwargs)

    def _internal_ask(self, base_ind: IndividualLike) -> IndividualLike:
        if len(self.inds_to_ask) > 0:
            base_ind[:] = self.inds_to_ask.pop()[:]
            base_ind.optim_flags['reeval'] = True
            return base_ind
        else:
#            return self.base_alg.ask()
            tmp = self.base_alg.ask()
            #print("DEBUG _internal_ask2:", tmp)
            base_ind[:] = tmp[:]
            return base_ind


    def _tell_sampling(self, ind: IndividualLike) -> bool:
        added = self.base_alg.tell(ind)
        self._tell_after_container_update(ind, added)
        return added


    def tell(self, individual: IndividualLike, fitness: Optional[Any] = None,
            features: Optional[FeaturesLike] = None, elapsed: Optional[float] = None) -> bool:
        """Give the algorithm an evaluated `individual` (with a valid fitness function)
        and store it into the container.

        Parameters
        ----------
        :param individual: IndividualLike
            The `individual` provided to the algorithm.
        :param fitness: Optional[FitnessLike]
            If provided, set `individual.fitness` to `fitness`.
        :param features: Optional[FeaturesLike]
            If provided, set `individual.features` to `features`.
        :param elapsed: Optional[float]
            If provided, set `individual.elapsed` to `elapsed`.
            It corresponds to the time (in seconds) elapsed during evaluation.
        """
        ind = copy.deepcopy(individual)
        if fitness is not None:
            if isinstance(fitness, FitnessLike):
                ind.fitness = fitness
            else:
                ind.fitness.values = fitness
        if len(ind.fitness.values) != self.base_alg._nb_objectives:
            raise ValueError(f"`individual` must possess a `fitness` attribute of dimension {self.base_alg._nb_objectives} (specified through parameter `nb_objectives` when creating a QDAlgorithm sub-class), instead of {len(individual.fitness.values)}.")
        if features is not None:
            ind.features[:] = features
        if elapsed is not None:
            ind.elapsed = elapsed
        #print("TELL: ", ind, ind.fitness, ind.fitness.nb_samples, ind.features, ind.features.nb_samples)

        if ind.nb_samples == 0:
            ind.nb_samples = 1 # XXX HACK

        try:
            if ind in self.container:
                index = self.container.index(ind)
                ind_in_grid = self.container[index]
                self.container.discard(ind_in_grid)
                ind.merge(ind_in_grid)
                added: bool = self.container.update([ind]) == 1
                if ind.optim_flags.get('reeval', False):
                    self._tell_after_container_update(ind, added)
                    #self._nb_updated_self += 1
                    #self._nb_evaluations_self += 1
                else:
                    added = self._tell_sampling(ind)
            else:
                added = self._tell_sampling(ind)
        except Exception as e:
            print(f"Exception during _tell_sampling: {e}")
            raise e

#        # XXX HACK
#        remaining_suggestions = self.budget - self.nb_suggestions
#        if len(self.inds_to_ask) > remaining_suggestions:
#            self.budget += len(self.inds_to_ask) - remaining_suggestions
#        #print("DEBUG tell: ", self.budget, self.nb_evaluations, self.base_alg.nb_evaluations, self.nb_suggestions, self.base_alg.nb_suggestions, len(self.staging_inds), len(self.inds_to_ask))
        return added



    def optimise(self, evaluate: Callable, budget: Optional[int] = None, 
            batch_mode: bool = True, executor: Optional[ExecutorLike] = None,
            send_several_suggestions_to_fn: bool = False) -> IndividualLike:
        optimisation_start_time: float = timer()
        self.base_alg_batch_start = optimisation_start_time
        # Call base_alg callback functions
        for fn in self.base_alg._callbacks.get("started_optimisation"):
            fn(self.base_alg)
        res = super().optimise(evaluate, budget, batch_mode, executor, send_several_suggestions_to_fn)
        # Call base_alg callback functions
        optimisation_elapsed: float = timer() - optimisation_start_time
        for fn in self.base_alg._callbacks.get("finished_optimisation"):
            fn(self.base_alg, optimisation_elapsed)
        return res


    def _verify_if_finished_iteration(self, batch_start_time: float) -> bool:
        res = super()._verify_if_finished_iteration(batch_start_time)
        if self.base_alg._nb_evaluations_in_iteration != 0:
            base_alg_res = self.base_alg._verify_if_finished_iteration(self.base_alg_batch_start) # XXX batch_start_time
            if base_alg_res:
                self.base_alg_batch_start = timer()
        return res




@registry.register
class ConditionalSamplingDecorator(SamplingDecorator):
    """Sampling decorator that switches between sampling types depending on container fullness. """

    def __init__(self, base_alg: QDAlgorithm, sampling1: SamplingDecorator, sampling2: SamplingDecorator, threshold_fullness = 0.999, min_samples = 1, **kwargs):
        self.sampling1 = sampling1
        self.sampling2 = sampling2
        self.threshold_fullness = threshold_fullness
        self.min_samples = min_samples
        self.has_switched = False
        self.current = self.sampling1
        self._nb_updated_self = 0
        self._nb_evaluations_self = 0
        self._nb_suggestions_self = 0
        super().__init__(base_alg, **kwargs)


    def _resample(self):
        """ Resample every ind to have at least `min_samples` samples"""
        for ind in self.base_alg.container:
            for _ in range(self.min_samples - ind.nb_samples):
                self.inds_to_ask.append(ind)

    def _update_counters(self, alg):
#        self._nb_updated = self.sampling1._nb_updated + self.sampling2._nb_updated + self._nb_updated_self
#        #self._nb_updated_in_iteration = self.current._nb_updated_in_iteration
#        self._nb_evaluations = self.sampling1._nb_evaluations + self.sampling2._nb_evaluations + self._nb_evaluations_self
#        #self._nb_evaluations_in_iteration = self.current._nb_evaluations_in_iteration
#        self._nb_suggestions = self.sampling1._nb_suggestions + self.sampling2._nb_suggestions + self._nb_suggestions_self
#        #self._nb_suggestions_in_iteration = self.current._nb_suggestions_in_iteration 
#        #print("DEBUG:", self._nb_suggestions, self.sampling1._nb_suggestions, self.sampling2._nb_suggestions, self._nb_suggestions_self)
#        #print("DEBUG:", self._nb_suggestions_in_iteration, self.sampling1._nb_suggestions_in_iteration, self.sampling2._nb_suggestions_in_iteration)
        pass


    def _internal_ask(self, base_ind: IndividualLike) -> IndividualLike:
        if len(self.inds_to_ask) > 0:
            base_ind[:] = self.inds_to_ask.pop()[:]
            base_ind.optim_flags['reeval'] = True
            self._nb_suggestions_self += 1
            self._update_counters(self.current)
            return base_ind
        else:
            fullness = self.base_alg.container.size / self.base_alg.container.capacity
            if self.has_switched or fullness >= self.threshold_fullness:
                if not self.has_switched:
                    self.has_switched = True
                    self._resample()
                    print(f"HAS SWITCHED !! {fullness} {self.threshold_fullness}")
                self.current = self.sampling2
            else:
                self.current = self.sampling1
            #res = self.current._internal_ask(base_ind)
            res = self.current.ask()
            self._update_counters(self.current)
            return res


    def _tell_sampling(self, ind: IndividualLike) -> bool:
        added = self.current._tell_sampling(ind)
        self._tell_after_container_update(ind, added)
        if added:
            self._nb_updated_self += 1
        self._nb_evaluations_self += 1
        self._update_counters(self.current)
        return added



@registry.register
class AlternatingMetaSamplingDecorator(SamplingDecorator):
    """Sampling decorator that switches between sampling types at every step. """

    def __init__(self, base_alg: QDAlgorithm, sd: List[SamplingDecorator], **kwargs):
        self.sd = sd
        assert(len(sd)>0)
        self.idx_current = 0
        self.current = self.sd[self.idx_current]
        super().__init__(base_alg, **kwargs)

    def _internal_ask(self, base_ind: IndividualLike) -> IndividualLike:
        if len(self.inds_to_ask) > 0:
            base_ind[:] = self.inds_to_ask.pop()[:]
            base_ind.optim_flags['reeval'] = True
            self._nb_suggestions_self += 1
            return base_ind
        else:
            self.idx_current = (self.idx_current+1) % len(self.sd)
            self.current = self.sd[self.idx_current]
            res = self.current.ask()
            return res

    def _tell_sampling(self, ind: IndividualLike) -> bool:
        added = self.current._tell_sampling(ind)
        self._tell_after_container_update(ind, added)
        return added




@registry.register
class FixedSamplingDecorator(SamplingDecorator):
    """TODO"""

    def __init__(self, base_alg: QDAlgorithm, nb_samples = 1, **kwargs):
        self.nb_samples = nb_samples
        super().__init__(base_alg, **kwargs)
        #self.budget = base_alg.budget # * nb_samples


    def _tell_sampling(self, ind: IndividualLike) -> bool:
        if self.nb_samples == 1:
            added = self.base_alg.tell(ind)
            self._tell_after_container_update(ind, added)
            return added

        if ind in self.staging_inds:
            idx = self.staging_inds.index(ind)
            #tmp_nb_samples = self.staging_inds[idx].nb_samples # XXX
            self.staging_inds[idx].merge(ind)
            ind = self.staging_inds[idx]
            #print("DEBUG staging: ", ind.nb_samples, tmp_nb_samples)
            if ind.nb_samples >= self.nb_samples:
                added = self.base_alg.tell(ind)
                self._tell_after_container_update(ind, added)
                self.staging_inds.remove(ind)
                return added

        else:
            self.staging_inds.append(ind)
            for _ in range(self.nb_samples - ind.nb_samples):
                self.inds_to_ask.append(ind)
            #self.budget += self.nb_samples - ind.nb_samples # XXX
        self._tell_after_container_update(ind, False)
        return False



@registry.register
class TimeBasedSamplingDecorator(SamplingDecorator):
    """TODO"""

    def __init__(self, base_alg: QDAlgorithm, min_samples = 5, max_samples = 100, **kwargs):
        self.min_samples = min_samples
        self.max_samples = max_samples
        super().__init__(base_alg, **kwargs)
#        self.budget = base_alg.budget


    def _tell_sampling(self, ind: IndividualLike) -> bool:
        if ind not in self.staging_inds:
            nb_samples = int(self.min_samples + (self.max_samples - self.min_samples) * (self.base_alg.nb_evaluations / self.budget))
            nb_samples = max(0, nb_samples - ind.nb_samples)
            ind.max_samples = nb_samples + ind.nb_samples

            if ind.nb_samples >= ind.max_samples:
                added = self.base_alg.tell(ind)
                self._tell_after_container_update(ind, added)
                return added
            else:
                self.staging_inds.append(ind)
                for _ in range(nb_samples):
                    self.inds_to_ask.append(ind)
#                self.budget += nb_samples # - 1
                #print("debug nbsamples", nb_samples, self.max_samples-self.min_samples, self.base_alg.nb_evaluations, self.base_alg.budget, self.nb_evaluations, self.nb_suggestions, self.budget)

        else:
            idx = self.staging_inds.index(ind)
            tmp_nb_samples = self.staging_inds[idx].nb_samples # XXX
            self.staging_inds[idx].merge(ind)
            ind = self.staging_inds[idx]
            #print("DEBUG staging: ", ind.nb_samples, tmp_nb_samples)
            if ind.nb_samples >= ind.max_samples:
                added = self.base_alg.tell(ind)
                self._tell_after_container_update(ind, added)
                self.staging_inds.remove(ind)
                return added

        self._tell_after_container_update(ind, False)
        return False




@registry.register
class StrengthBasedSamplingDecorator(SamplingDecorator):
    """TODO"""

    def __init__(self, base_alg: QDAlgorithm, min_samples = 5, max_samples = 100, update_max_stat_every_it = True, **kwargs):
        self.min_samples = min_samples
        self.max_samples = max_samples
        super().__init__(base_alg, **kwargs)
#        self.budget = base_alg.budget * min_samples
        self.update_max_stat_every_it = update_max_stat_every_it
        if self.update_max_stat_every_it:
            self.base_alg.add_callback("iteration", self._sampled_iteration_callback)
            #self.add_callback("iteration", self._sampled_iteration_callback)
        else:
            self.base_alg.add_callback("tell", self._sampled_iteration_callback)
            #self.add_callback("tell", self._sampled_iteration_callback)
        self._sampled_iteration_callback(self, 0.)


    def _sampled_iteration_callback(self, algo, batch_elapsed):
        self.max_stat = self._compute_max_stat()

    def _compute_max_stat(self):
        return 0

    def _compute_stat(self, ind):
        return 0


    def _tell_sampling(self, ind: IndividualLike) -> bool:
#        print("DEBUG tell_sampling: ", self.budget, self.nb_evaluations, self.base_alg.nb_evaluations, self.nb_suggestions, self.base_alg.nb_suggestions, len(self.staging_inds), len(self.inds_to_ask))

        if ind not in self.staging_inds:
            ind.max_samples = 0
            self.staging_inds.append(ind)
            for _ in range(self.min_samples - ind.nb_samples):
                self.inds_to_ask.append(ind)
            #self._tell_after_container_update(ind, False)
            #print("DEBUG4 ", self.base_alg.nb_evaluations, self.base_alg.nb_suggestions, self.nb_evaluations, self.nb_suggestions, self.budget, len(self.staging_inds), len(self.inds_to_ask))
            idx = self.staging_inds.index(ind)
        else:
            idx = self.staging_inds.index(ind)
            self.staging_inds[idx].merge(ind)
        ind = self.staging_inds[idx]

        if ind.nb_samples < self.min_samples:
#            print("DEBUG1 ", self.base_alg.nb_evaluations, self.base_alg.nb_suggestions, self.nb_evaluations, self.nb_suggestions, self.budget, len(self.staging_inds), len(self.inds_to_ask))
            self._tell_after_container_update(ind, False)
            return False

        else:
#                if ind.max_samples == 0:
#                    nb_samples = 1
#                    ind.max_samples = 6 # nb_samples + ind.nb_samples
#                    for _ in range(nb_samples):
#                        self.inds_to_ask.append(ind)
#                    self.budget += nb_samples # - 1
#                    ##print("DEBUG5 ", nb_samples, ind.max_samples, ind.nb_samples, self.budget)

            if ind.max_samples == 0:
                stat = self._compute_stat(ind)

                # Set nb_samples
                if self.max_stat == 0:
                    nb_samples = 1.0
                elif self.max_stat == 1:
                    nb_samples = max(0.5, stat / self.max_stat)
                else:
                    nb_samples = stat / self.max_stat
#                print("debug NB_SAMPLES", nb_samples, max(0, int(self.min_samples + (self.max_samples - self.min_samples) * nb_samples) - ind.nb_samples), ind.nb_samples, ind.max_samples)
                nb_samples = max(0, int(self.min_samples + (self.max_samples - self.min_samples) * nb_samples) - ind.nb_samples)
                ind.max_samples = nb_samples + ind.nb_samples
                for _ in range(nb_samples):
                    self.inds_to_ask.append(ind)
#                self.budget += nb_samples # - 1
                #idx2 = self.staging_inds.index(ind)
#                    print("debug nbsamples", stat, self.max_stat, ind.max_samples, nb_samples, self.max_samples-self.min_samples, self.base_alg.nb_evaluations, self.base_alg.budget, self.nb_evaluations, self.nb_suggestions, self.budget)

            if ind.nb_samples >= ind.max_samples:
                self.staging_inds.remove(ind)
                added = self.base_alg.tell(ind)
                self._tell_after_container_update(ind, added)
#                print("DEBUG2 ", self.base_alg.nb_evaluations, self.base_alg.nb_suggestions, self.nb_evaluations, self.nb_suggestions, self.budget, len(self.staging_inds), len(self.inds_to_ask), ind.nb_samples, ind.max_samples)
                return added
            else:
#                print("DEBUG3 ", self.base_alg.nb_evaluations, self.base_alg.nb_suggestions, self.nb_evaluations, self.nb_suggestions, self.budget, len(self.staging_inds), len(self.inds_to_ask), ind.nb_samples, ind.max_samples)
                self._tell_after_container_update(ind, False)
                return False

        return False





# NOTE : must be used with batch_mode = True, as it only recompute max_dom every iteration
@registry.register
class DominationStrengthSamplingDecorator(StrengthBasedSamplingDecorator):
    """TODO"""

    def _compute_max_stat(self):
        grid = self.container
        max_dom = 0
        for i2 in grid:
            # Compute max score of dominance in the container
            current_dom = 0
            for i3 in grid:
                if i2.dominates(i3):
                    current_dom += 1
            max_dom = max(max_dom, current_dom)
        return max_dom

    def _compute_stat(self, ind):
        # Compute number of solutions dominated by ``ind`` in the container
        nb_dominated = 0
        for i2 in self.container:
            if ind.dominates(i2):
                nb_dominated += 1
        return nb_dominated



# NOTE : must be used with batch_mode = True, as it only recompute max_dom every iteration
@registry.register
class ActivityStrengthSamplingDecorator(StrengthBasedSamplingDecorator):
    """TODO"""

    def _compute_max_stat(self):
        return np.max(self.container.activity_per_bin)

    def _compute_stat(self, ind):
        # Compute number of solutions dominated by ``ind`` in the container
        try:
            ig = self.container.index_grid(ind.features)
            activity = 0 if ig is None else self.container.activity_per_bin[ig]
        except Exception as e:
            ig = None
            activity = 0
        if np.isnan(activity):
            activity = 0
        return activity




@registry.register
class SEDRDecorator(SamplingDecorator):
    """TODO"""

    def __init__(self, base_alg: QDAlgorithm, min_r_fitness = 0.0, max_r_fitness = 0.05, min_r_features = 0.0, max_r_features = 0.05, min_samples = 5, max_samples = 100, **kwargs):
        self.min_r_fitness = min_r_fitness
        self.max_r_fitness = max_r_fitness
        self.min_r_features = min_r_features
        self.max_r_features = max_r_features
        self.min_samples = min_samples
        self.max_samples = max_samples
        super().__init__(base_alg, **kwargs)
#        self.budget = base_alg.budget * min_samples


    def _tell_sampling(self, ind: IndividualLike) -> bool:
        if ind in self.staging_inds:
            idx = self.staging_inds.index(ind)
            tmp_nb_samples = self.staging_inds[idx].nb_samples

            self.staging_inds[idx].merge(ind)
            ind = self.staging_inds[idx]
            #print("DEBUG staging: ", ind.nb_samples, tmp_nb_samples)
            if ind.nb_samples < self.min_samples:
                self._tell_after_container_update(ind, False)
                return False

            sem_fitness = scipy.stats.sem(ind.fitness.samples)
            sem_features = scipy.stats.sem(ind.features.samples)
            #print("DEBUG sem", sem_fitness, sem_features, ind.nb_samples, self.max_samples)
            if ind.nb_samples >= self.max_samples or (np.all(sem_fitness >= self.min_r_fitness) and np.all(sem_fitness <= self.max_r_fitness) and np.all(sem_features >= self.min_r_fitness) and np.all(sem_features <= self.max_r_fitness)):
                added = self.base_alg.tell(ind)
                self._tell_after_container_update(ind, added)
                self.staging_inds.remove(ind)
                return added
            else:
                self.inds_to_ask.append(ind)
#                self.budget += 1
                self._tell_after_container_update(ind, False)
                return False

        else:
            self.staging_inds.append(ind)
            for _ in range(self.min_samples - ind.nb_samples):
                self.inds_to_ask.append(ind)
            self._tell_after_container_update(ind, False)
        return False



@registry.register
class FBDRDecorator(SamplingDecorator):
    """TODO"""

    def __init__(self, base_alg: QDAlgorithm, min_samples = 5, max_samples = 100, **kwargs):
#        self.min_features = np.array(self.container.features_domain)[:,0]
#        self.max_features = np.array(self.container.features_domain)[:,1]
        self.min_samples = min_samples
        self.max_samples = max_samples
        super().__init__(base_alg, **kwargs)
#        self.budget = base_alg.budget * min_samples
        self.min_fitness = np.array(self.container.fitness_domain)[:,0]
        self.max_fitness = np.array(self.container.fitness_domain)[:,1]
        self.diff_fitness = self.max_fitness - self.min_fitness


    def _tell_sampling(self, ind: IndividualLike) -> bool:
        if ind not in self.staging_inds:
            self.staging_inds.append(ind)
            for _ in range(self.min_samples - ind.nb_samples):
                self.inds_to_ask.append(ind)
            idx = self.staging_inds.index(ind)

        else:
            idx = self.staging_inds.index(ind)
            #tmp_nb_samples = self.staging_inds[idx].nb_samples
            self.staging_inds[idx].merge(ind)
        ind = self.staging_inds[idx]

        #print("DEBUG staging: ", ind.nb_samples, tmp_nb_samples)
        if ind.nb_samples < self.min_samples:
            self._tell_after_container_update(ind, False)
            return False

        norm_fit_estimate = 1. - np.array(ind.fitness) / self.diff_fitness
        avg_norm_fit_estimate = np.mean(norm_fit_estimate)
        #nb_samples = (2. * max(0.5, avg_norm_fit_estimate))**self.alpha * (self.max_samples - self.min_samples)
        nb_samples = self.min_samples + avg_norm_fit_estimate * (self.max_samples - self.min_samples)

        #print("DEBUG sem", sem_fitness, sem_features, ind.nb_samples, self.max_samples)
        if ind.nb_samples >= nb_samples:
            added = self.base_alg.tell(ind)
            self._tell_after_container_update(ind, added)
            self.staging_inds.remove(ind)
            return added
        else:
            self.inds_to_ask.append(ind)
#            self.budget += 1
            self._tell_after_container_update(ind, False)
            return False

        return False



@registry.register
class FBSEDRDecorator(SEDRDecorator):
    """TODO"""

    def __init__(self, base_alg: QDAlgorithm, max_r_fitness = 0.05, max_r_features = 0.05, min_samples = 5, max_samples = 100, **kwargs):
        super().__init__(base_alg, 0., max_r_fitness, 0., max_r_features, min_samples, max_samples, **kwargs)
#        self.budget = base_alg.budget * min_samples
        self.min_fitness = np.array(self.container.fitness_domain)[:,0]
        self.max_fitness = np.array(self.container.fitness_domain)[:,1]
        self.diff_fitness = self.max_fitness - self.min_fitness


    def _tell_sampling(self, ind: IndividualLike) -> bool:
        if ind in self.staging_inds:
            idx = self.staging_inds.index(ind)
            tmp_nb_samples = self.staging_inds[idx].nb_samples

            self.staging_inds[idx].merge(ind)
            ind = self.staging_inds[idx]
            #print("DEBUG staging: ", ind.nb_samples, tmp_nb_samples)
            if ind.nb_samples < self.min_samples:
                self._tell_after_container_update(ind, False)
                return False

            #sem_fitness = scipy.stats.sem(ind.fitness.samples)
            #sem_features = scipy.stats.sem(ind.features.samples)
            std_fitness = np.max(np.std(ind.fitness.samples, axis=0))
            std_features = np.max(np.std(ind.features.samples, axis=0))
            nb_add_sedr = max((std_fitness/self.max_r_fitness)**2., (std_features/self.max_r_features)**2.)

            norm_fit_estimate = 1. - np.array(ind.fitness) / self.diff_fitness
            avg_norm_fit_estimate = np.mean(norm_fit_estimate)
            #nb_samples = (2. * max(0.5, avg_norm_fit_estimate))**self.alpha * (self.max_samples - self.min_samples)
            nb_add_fbdr = avg_norm_fit_estimate * (self.max_samples - self.min_samples)
            nb_samples = int(self.min_samples + (nb_add_fbdr + nb_add_sedr) / 2.)

            #print("DEBUG: ", std_fitness, std_features, ind.nb_samples, nb_add_sedr, nb_add_fbdr, nb_samples)
            if ind.nb_samples >= nb_samples or ind.nb_samples >= self.max_samples:
                added = self.base_alg.tell(ind)
                self._tell_after_container_update(ind, added)
                self.staging_inds.remove(ind)
                return added
            else:
                self.inds_to_ask.append(ind)
#                self.budget += 1
                self._tell_after_container_update(ind, False)
                return False

        else:
            self.staging_inds.append(ind)
            for _ in range(self.min_samples - ind.nb_samples):
                self.inds_to_ask.append(ind)
            self._tell_after_container_update(ind, False)
        return False





@registry.register
class LinearDSStderrDecorator(SEDRDecorator):
    """TODO"""

    def __init__(self, base_alg: QDAlgorithm, min_r_fitness = 0.0, max_r_fitness = 0.05, min_r_features = 0.0, max_r_features = 0.05, min_samples = 3, max_samples = 100, **kwargs):
        super().__init__(base_alg, min_r_fitness, max_r_fitness, min_r_features, max_r_features, min_samples, max_samples, **kwargs)


    def _tell_sampling(self, ind: IndividualLike) -> bool:
        if ind in self.staging_inds:
            idx = self.staging_inds.index(ind)
            tmp_nb_samples = self.staging_inds[idx].nb_samples

            self.staging_inds[idx].merge(ind)
            ind = self.staging_inds[idx]
            #print("DEBUG staging: ", ind.nb_samples, tmp_nb_samples)
            if ind.nb_samples < self.min_samples:
                self._tell_after_container_update(ind, False)
                return False
            if ind.nb_samples < ind.nb_samples_before_next_batch:
                self._tell_after_container_update(ind, False)
                return False

            sem_fitness = scipy.stats.sem(ind.fitness.samples)
            sem_features = scipy.stats.sem(ind.features.samples)
            budget_samples_fitness = self.min_samples + (self.max_samples - self.min_samples) * (min(np.max(sem_fitness), self.max_r_fitness) - self.min_r_fitness) / (self.max_r_fitness - self.min_r_fitness)
            budget_samples_features = self.min_samples + (self.max_samples - self.min_samples) * (min(np.max(sem_features), self.max_r_features) - self.min_r_features) / (self.max_r_features - self.min_r_features)
            budget_samples = int(max(budget_samples_fitness, budget_samples_features))
            #print("DEBUG", budget_samples, budget_samples_fitness, sem_fitness, (np.max(sem_fitness) - self.min_r_fitness), (np.max(sem_fitness) - self.min_r_fitness) / (self.max_r_fitness - self.min_r_fitness))

            #print("DEBUG sem", sem_fitness, sem_features)
            if ind.nb_samples >= self.max_samples or (np.all(sem_fitness >= self.min_r_fitness) and np.all(sem_fitness <= self.max_r_fitness) and np.all(sem_features >= self.min_r_fitness) and np.all(sem_features <= self.max_r_fitness)):
                added = self.base_alg.tell(ind)
                self._tell_after_container_update(ind, added)
                self.staging_inds.remove(ind)
                return added
            else:
                samples_to_add = min(budget_samples - ind.nb_samples, self.max_samples)
                for _ in range(samples_to_add):
                    self.inds_to_ask.append(ind)
#                self.budget += samples_to_add
                self._tell_after_container_update(ind, False)
                return False

        else:
            ind.nb_samples_before_next_batch = ind.nb_samples
            self.staging_inds.append(ind)
            for _ in range(self.min_samples - ind.nb_samples):
                self.inds_to_ask.append(ind)
            ind.nb_samples_before_next_batch += self.min_samples - ind.nb_samples
            self._tell_after_container_update(ind, False)
        return False







@registry.register
class AdaptiveSamplingDecorator(SamplingDecorator):
    """TODO"""

    def __init__(self, base_alg: QDAlgorithm, min_samples = 5, max_samples = 100, **kwargs):
        self.min_samples = min_samples
        self.max_samples = max_samples
#        self.black_list = []
        super().__init__(base_alg, **kwargs)
#        self.budget = base_alg.budget * min_samples


    def _tell_sampling(self, ind: IndividualLike) -> bool:
#        if ind in self.black_list:
#            return False

        #if ind in self.container:
        #    index = self.container.index(ind)
        #    ind_in_grid = self.container[index]
        #    #ig = self.container.index_grid(ind.features)
        #    #self.container[index].merge(ind)
        #    #print("DEBUGa", index, ig, self.container.solutions[ig], self.container[index], ind)
        #    self.container.discard(ind_in_grid)
        #    #tmp_nb_samples = ind_in_grid.nb_samples # XXX
        #    ind.merge(ind_in_grid)
        #    added: bool = self.container.update([ind]) == 1
        #    if ind.optim_flags.get('reeval', False):
        #        self._tell_after_container_update(ind, added)
        #        #print("DEBUG in container: ", ind.nb_samples, tmp_nb_samples, added)
        #        return added

        is_staging_ind = False
        if ind in self.staging_inds:
            idx = self.staging_inds.index(ind)
            #tmp_nb_samples = self.staging_inds[idx].nb_samples # XXX
            self.staging_inds[idx].merge(ind)
            ind = self.staging_inds[idx]
            #print("DEBUG staging: ", ind.nb_samples, tmp_nb_samples)
            is_staging_ind = True
        else:
            self.staging_inds.append(ind)
            for _ in range(self.min_samples - ind.nb_samples):
                self.inds_to_ask.append(ind)
            #self._tell_after_container_update(ind, False)

        #print("STAGIN:", len(self.staging_inds))

        if ind.nb_samples < self.min_samples:
            self._tell_after_container_update(ind, False)
            return False

        can_be_added = False
        will_be_added = True
        try:
            index_g = self.container.index_grid(ind.features)
            elites = self.container.solutions[index_g]
        except Exception as e:
            index_g = None
        if index_g is None:
            # We may put `ind` in an empty bin
            can_be_added = True
#            print("TELL: empty")
        else:
            if len(elites) == 0:
                can_be_added = True
                #print("TELL: no elites")
            else:
                elite = elites[0] # TODO HACK !! ONLY WORKS IF THE ELITES ARE ALREADY SORTED 

                if ind.dominates(elite):
                    if ind.nb_samples >= elite.nb_samples or ind.nb_samples >= self.max_samples:
                        can_be_added = True
                        #print("TELL: enough repeats:", ind.nb_samples, elite.nb_samples)
                    else:
                        #if is_staging_ind:
                        #    #print("TELL: in staging")
                        #    pass
                        #else:
                        #    #print("REEVALS: ", elite.nb_samples, ind.nb_samples, elite.nb_samples - ind.nb_samples, len(self.inds_to_ask))
                        #    self.staging_inds.append(ind)
                        #    for _ in range(elite.nb_samples - ind.nb_samples):
                        #        self.inds_to_ask.append(ind)
                        #        self.budget += 1
                        #        #self.base_alg.budget += 1
                        #    self.base_alg.tell(ind)
                        #    self._tell_after_container_update(ind, False)
                        #    return False
                        self.inds_to_ask.append(ind)
#                        self.budget += 1
                        #self.base_alg.tell(ind)
                        self._tell_after_container_update(ind, False)
                        return False
                else:
                    can_be_added = True
                    will_be_added = False
                    if elite.nb_samples < self.max_samples:
                        self.inds_to_ask.append(elite)
#                        self.budget += 1
                        #print("TELL: add elite", len(self.inds_to_ask))

#        added: bool = self.container.update([ind]) == 1
#        self.base_alg._tell_after_container_update(ind, added)
#        if is_staging_ind:
#            self.staging_inds.remove(ind)
#        return added
        #print("DEBUG nb_samples: ", ind.nb_samples)
        self.base_alg.tell(ind)
#        if is_staging_ind:
#            self.staging_inds.remove(ind)
        self._tell_after_container_update(ind, will_be_added)
        self.staging_inds.remove(ind)
        #self._nb_updated = self.base_alg._nb_updated
        #self._nb_updated_in_iteration = self.base_alg._nb_updated_in_iteration

#        if not will_be_added:
#            self.black_list.append(ind)
        #print("DEBUG: ", self.nb_suggestions, self.nb_evaluations, self.budget, self.base_alg.nb_suggestions, self.base_alg.nb_evaluations, self.base_alg.budget)
        return will_be_added




@registry.register
class ConfidenceBasedAdaptiveSamplingDecorator(SamplingDecorator):
    """TODO"""

    def __init__(self, base_alg: QDAlgorithm, min_samples = 5, max_samples = 100, alpha = 0.05, **kwargs):
        self.min_samples = min_samples
        self.max_samples = max_samples
        self.alpha = alpha
        super().__init__(base_alg, **kwargs)
#        self.budget = base_alg.budget * min_samples


    def _tell_sampling(self, ind: IndividualLike) -> bool:
        #if ind in self.container:
        #    index = self.container.index(ind)
        #    ind_in_grid = self.container[index]
        #    #ig = self.container.index_grid(ind.features)
        #    #self.container[index].merge(ind)
        #    #print("DEBUGa", index, ig, self.container.solutions[ig], self.container[index], ind)
        #    self.container.discard(ind_in_grid)
        #    #tmp_nb_samples = ind_in_grid.nb_samples # XXX
        #    ind.merge(ind_in_grid)
        #    added: bool = self.container.update([ind]) == 1
        #    if ind.optim_flags.get('reeval', False):
        #        self._tell_after_container_update(ind, added)
        #        return added
        #    #else:
        #    #    print("DEBUG !reeval")
        #    #else:
        #    #    added = self.base_alg.tell(ind)
        #    #self._tell_after_container_update(ind, added)
        #    ##self.base_alg._tell_after_container_update(ind, added)
        #    ##print("DEBUG in container: ", ind.nb_samples, tmp_nb_samples, added)
        #    #return added

        is_staging_ind = False
        if ind in self.staging_inds:
            idx = self.staging_inds.index(ind)
            #tmp_nb_samples = self.staging_inds[idx].nb_samples # XXX
            self.staging_inds[idx].merge(ind)
            ind = self.staging_inds[idx]
            #print("DEBUG staging: ", ind.nb_samples, tmp_nb_samples)
            is_staging_ind = True
        else:
            self.staging_inds.append(ind)
            for _ in range(self.min_samples - ind.nb_samples):
                self.inds_to_ask.append(ind)
            #self._tell_after_container_update(ind, False)

        if ind.nb_samples < self.min_samples:
            self._tell_after_container_update(ind, False)
            return False

        can_be_added = False
        try:
            index_g = self.container.index_grid(ind.features)
            elites = self.container.solutions[index_g]
        except Exception as e:
            index_g = None
        if index_g is None:
            # We may put `ind` in an empty bin
            can_be_added = True
#            print("TELL: empty")
        else:
            if len(elites) == 0:
                can_be_added = True
                #print("TELL: no elites")
            else:
                elite = elites[0] # TODO HACK !! ONLY WORKS IF THE ELITES ARE ALREADY SORTED 

                # Perform Welch confidence interval t-test between ``ind`` and ``elite``
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    below_alpha = True
                    ind_fit_std = np.std(np.array(ind.fitness.samples), axis=0)
                    elite_fit_std = np.std(np.array(elite.fitness.samples), axis=0)
                    ind_feat_std = np.std(np.array(ind.features.samples), axis=0)
                    elite_feat_std = np.std(np.array(elite.features.samples), axis=0)
                    for i in range(len(ind.fitness)):
                        s, pv = scipy.stats.ttest_ind_from_stats(
                                mean1=ind.fitness[i], std1=ind_fit_std[i], nobs1=ind.fitness.nb_samples,
                                mean2=elite.fitness[i], std2=elite_fit_std[i], nobs2=elite.fitness.nb_samples)
                        #print(f"fit s={s} pv={pv}")
                        if pv >= self.alpha:
                            below_alpha = False
                            break
                    if below_alpha:
                        for i in range(len(ind.features)):
                            s, pv = scipy.stats.ttest_ind_from_stats(
                                    mean1=ind.features[i], std1=ind_feat_std[i], nobs1=ind.features.nb_samples,
                                    mean2=elite.features[i], std2=elite_feat_std[i], nobs2=elite.features.nb_samples)
                            #print(f"feat s={s} pv={pv}")
                            if pv >= self.alpha:
                                below_alpha = False
                                break

                if below_alpha:
                    can_be_added = True
                else:
                    ind_max_std = max(np.max(ind_fit_std), np.max(ind_feat_std))
                    elite_max_std = max(np.max(elite_fit_std), np.max(elite_feat_std))
                    to_reeval = None
                    if ind_max_std >= elite_max_std:
                        if ind.nb_samples < self.max_samples:
                            to_reeval = ind
                        else:
                            if elite.nb_samples < self.max_samples:
                                to_reeval = elite
                            else:
                                to_reeval = None
                    else:
                        if elite.nb_samples < self.max_samples:
                            to_reeval = elite
                        else:
                            if ind.nb_samples < self.max_samples:
                                to_reeval = ind
                            else:
                                to_reeval = None
                    if to_reeval is None:
                        can_be_added = True
                    else:
                        #print("DEBUG nb_samples:", to_reeval.nb_samples, ind.nb_samples, elite.nb_samples, self.max_samples)
                        self.inds_to_ask.append(to_reeval)
#                        self.budget += 1
                        self._tell_after_container_update(ind, False)
                        #print("TELL: add reeval", len(self.inds_to_ask))
                        return False

#                # XXX
#                if below_alpha:
#                    can_be_added = True
#                else:
#                    ind_max_std = max(np.max(ind_fit_std), np.max(ind_feat_std))
#                    elite_max_std = max(np.max(elite_fit_std), np.max(elite_feat_std))
#                    to_reeval = None
#                    if ind_max_std >= elite_max_std:
#                        if ind.nb_samples < self.max_samples:
#                            to_reeval = ind
#                        else:
#                            if elite.nb_samples < self.max_samples:
#                                to_reeval = None
#                            else:
#                                to_reeval = None
#                    else:
#                        if elite.nb_samples < self.max_samples:
#                            to_reeval = None
#                        else:
#                            if ind.nb_samples < self.max_samples:
#                                to_reeval = ind
#                            else:
#                                to_reeval = None
#                    if to_reeval is None:
#                        can_be_added = True
#                    else:
#                        #print("DEBUG nb_samples:", to_reeval.nb_samples, ind.nb_samples, elite.nb_samples, self.max_samples)
#                        self.inds_to_ask.append(to_reeval)
#                        self.budget += 1
#                        self._tell_after_container_update(ind, False)
#                        #print("TELL: add reeval", len(self.inds_to_ask))
#                        return False


        added = self.base_alg.tell(ind)
        self._tell_after_container_update(ind, added)
        self.staging_inds.remove(ind)

#        print("DEBUG: ", self.nb_suggestions, self.nb_evaluations, self.budget, self.base_alg.nb_suggestions, self.base_alg.nb_evaluations, self.base_alg.budget)
        return added




# TODO Finish, Debug and Refactor
@registry.register
class NoiseAnalysisSamplingDecorator(SamplingDecorator):
    """TODO"""

    def __init__(self, base_alg: QDAlgorithm, min_samples = 5, max_samples = 100, **kwargs):
        self.min_samples = min_samples
        self.max_samples = max_samples
        super().__init__(base_alg, **kwargs)
#        self.budget = base_alg.budget * min_samples
        base_alg.add_callback("iteration", self._sampled_iteration_callback)
        self._sampled_iteration_callback(self, 0.)


    def _sampled_iteration_callback(self, algo, batch_elapsed):
        # Compute variances of all stored individuals
        # Sum stddev: sqrt(sum of variances), cf: https://stats.stackexchange.com/questions/25848/how-to-sum-a-standard-deviation
        self.inds_fit_var = np.array(np.sqrt(np.sum([np.var(ind.fitness.samples, axis=0) for ind in algo.container], axis=0))).flatten()
        self.inds_feat_var = np.array(np.sqrt(np.sum([np.var(ind.features.samples, axis=0) for ind in algo.container], axis=0))).flatten()
        self.inds_var = list(self.inds_fit_var) + list(self.inds_feat_var)
        #print("DEBUG noise:", self.inds_fit_var, self.inds_feat_var, self.inds_var)


    def _tell_sampling(self, ind: IndividualLike) -> bool:
        is_staging_ind = False
        if ind in self.staging_inds:
            idx = self.staging_inds.index(ind)
            tmp_nb_samples = self.staging_inds[idx].nb_samples # XXX
            self.staging_inds[idx].merge(ind)
            ind = self.staging_inds[idx]
            #print("DEBUG staging: ", ind.nb_samples, tmp_nb_samples)
            is_staging_ind = True
        else:
            ind.max_samples = self.min_samples
            self.staging_inds.append(ind)
            for _ in range(self.min_samples - ind.nb_samples):
                self.inds_to_ask.append(ind)
            self._tell_after_container_update(ind, False)

        #if ind.nb_samples < self.min_samples or ind.nb_samples < self.max_samples:
        if ind.nb_samples < self.min_samples:
            return False
        print("DEBUG ind: ", ind.nb_samples, self.min_samples, self.max_samples)

        can_be_added = False
        try:
            index_g = self.container.index_grid(ind.features)
            elites = self.container.solutions[index_g]
        except Exception as e:
            index_g = None
        if index_g is None:
            # We may put `ind` in an empty bin
            can_be_added = True
#            print("TELL: empty")
        else:
            if len(elites) == 0:
                can_be_added = True
                #print("TELL: no elites")
            else:
                elite = elites[0] # TODO HACK !! ONLY WORKS IF THE ELITES ARE ALREADY SORTED 

                diff_fit_ind_elite = [abs(ind.fitness[i] - elite.fitness[i]) for i in range(len(ind.fitness))]
                diff_feat_ind_elite = [abs(ind.features[i] - elite.features[i]) for i in range(len(ind.features))]
                diff_ind_elite = diff_fit_ind_elite + diff_feat_ind_elite
                min_fit_ind_elite = [min(ind.fitness[i], elite.fitness[i]) for i in range(len(ind.fitness))]
                min_feat_ind_elite = [min(ind.features[i], elite.features[i]) for i in range(len(ind.features))]
                min_ind_elite = min_fit_ind_elite + min_feat_ind_elite
                max_fit_ind_elite = [max(ind.fitness[i], elite.fitness[i]) for i in range(len(ind.fitness))]
                max_feat_ind_elite = [max(ind.features[i], elite.features[i]) for i in range(len(ind.features))]
                max_ind_elite = max_fit_ind_elite + max_feat_ind_elite

                omega = np.nan
                if len(self.inds_var) == len(diff_ind_elite):
                    for i, diff in enumerate(diff_ind_elite):
                        l = min_ind_elite[i]
                        h = max_ind_elite[i]
                        s2 = 2. * self.inds_var[i]
                        if diff <= s2:
                            omega = np.nanmax([omega, (l + s2 - (h - s2)) / (h + s2 - (l - s2))])
                            print("OMEGA: ", diff<=s2, omega, diff, s2, l, h)

                if np.isnan(omega):
                    can_be_added = True
                else:
                    nb_add = int((1.96 / (2. * (1. - omega)))**2.)
                    nb_add = max(nb_add, self.max_samples)
                    ind.max_samples = nb_add + ind.nb_samples
                    print("ADD: ", nb_add, ind.max_samples, omega)
                    for _ in range(nb_add):
                        self.inds_to_ask.append(ind)
#                    self.budget += nb_add
                    self._tell_after_container_update(ind, False)
                    return False

        added = self.base_alg.tell(ind)
        self._tell_after_container_update(ind, added)
        self.staging_inds.remove(ind)

#        print("DEBUG: ", self.nb_suggestions, self.nb_evaluations, self.budget, self.base_alg.nb_suggestions, self.base_alg.nb_evaluations, self.base_alg.budget)
        return added





@registry.register
class RandomSearchMutPolyBoundedForNoisyInds(Evolution):
    """TODO"""
    ind_domain: DomainLike
    sel_pb: float
    init_pb: float
    mut_pb: float
    eta: float

    def __init__(self, container: Container, budget: int,
            dimension: int, ind_domain: DomainLike = (0., 1.),
            sel_pb: float = 0.5, init_pb: float = 0.5, mut_pb: float = 0.2, eta: float = 20.,
            **kwargs):
        self.ind_domain = ind_domain
        self.sel_pb = sel_pb
        self.init_pb = init_pb
        self.mut_pb = mut_pb
        self.eta = eta
        base_ind_gen = gen_sampled_individuals()

        #init_fn = partial(np.random.uniform, ind_domain[0], ind_domain[1], dimension)
        init_fn = partial(lambda dim, base_ind: [random.uniform(ind_domain[0], ind_domain[1]) for _ in range(dim)], dimension)
        select_or_initialise = partial(tools.sel_or_init,
                sel_fn = tools.sel_random,
                sel_pb = sel_pb,
                init_fn = init_fn,
                init_pb = init_pb)
        #vary = partial(tools.mut_polynomial_bounded, low=ind_domain[0], up=ind_domain[1], eta=eta, mut_pb=mut_pb)
        vary = partial(self._vary, low=ind_domain[0], up=ind_domain[1], eta=eta, mut_pb=mut_pb)

        super().__init__(container, budget, dimension=dimension, # type: ignore
                select_or_initialise=select_or_initialise, vary=vary, base_ind_gen=base_ind_gen, **kwargs) # type: ignore


    def _vary(self, ind, **kwargs):
        mutated = tools.mut_polynomial_bounded(ind, **kwargs)
        mutated.reset()
        return mutated


@registry.register
class MutPolyBoundedForNoisyInds(RandomSearchMutPolyBoundedForNoisyInds):
    """TODO"""
    def __init__(self, container: Container, budget: int,
            dimension: int, ind_domain: DomainLike = (0., 1.),
            mut_pb: float = 0.2, eta: float = 20., **kwargs):
        super().__init__(container, budget, dimension=dimension,
                ind_domain=ind_domain,
                sel_pb=1.0, init_pb=0.0,
                mut_pb=mut_pb, eta=eta, **kwargs)




# MODELINE "{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
