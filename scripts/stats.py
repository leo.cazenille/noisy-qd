#!/usr/bin/env python3


from qdpy import algorithms, containers, benchmarks, plots

import numpy as np
import warnings
import os
import random



def iteration_callback(algo, batch_elapsed, grid_ref):
    global_reliability = compute_global_reliability(grid_ref, algo.container)
    algo.container.global_reliability.append(global_reliability)
    #print(f"global_reliability = {global_reliability}")

def sampled_iteration_callback(algo, batch_elapsed, grid_ref):
    global_reliability = compute_global_reliability_noisy(grid_ref, algo.container)
    if not hasattr(algo.container, 'global_reliability'):
        algo.container.global_reliability = []
    algo.container.global_reliability.append(global_reliability)
    #print(f"global_reliability = {global_reliability}")

    nb_samples, tot_nb_samples = compute_nb_samples(algo.container)
    if not hasattr(algo.container, 'nb_samples'):
        algo.container.nb_samples = []
    algo.container.nb_samples.append(nb_samples)
    if not hasattr(algo.container, 'tot_nb_samples'):
        algo.container.tot_nb_samples = []
    algo.container.tot_nb_samples.append(tot_nb_samples)
    #print(f"nb_samples = {nb_samples}")

def tell_callback(algo, ind, grid_ref):
    global_reliability = compute_global_reliability(grid_ref, algo.container)
    algo.container.global_reliability.append(global_reliability)
    #print(f"global_reliability = {global_reliability}")

#def sampled_tell_callback(algo, ind, grid_ref):
#    #global_reliability = compute_global_reliability(grid_ref, algo.container)
#    global_reliability = compute_global_reliability_noisy(grid_ref, algo.container)
#
#    if not hasattr(algo.container, 'global_reliability'):
#        algo.container.global_reliability = []
#    algo.container.global_reliability.append(global_reliability)
#
##    # XXX
##    nb_samples = ind.nb_samples
##    if ind in algo.container:
##        idx = algo.container.index(ind)
##        nb_samples += algo.container[idx].nb_samples
#    nb_samples, tot_nb_samples = compute_nb_samples(algo.container)
#    if not hasattr(algo.container, 'nb_samples'):
#        algo.container.nb_samples = []
#    algo.container.nb_samples.append(nb_samples)
#    #print(f"nb_samples = {nb_samples}")


def sampled_tell_callback(algo, ind, grid_ref, grid_noiseless, decorator):
    try:
        if grid_noiseless is not None and ind in algo.container and len(ind.features) > 0:
            ig = algo.container.index_grid(ind.features)
            old_ig = None
            for i in algo.container:
                i_ig = algo.container.index_grid(i.features)
                if ig == i_ig:
                    old_ig = i_ig
                    break
            if old_ig is not None:
                old_inds = grid_noiseless.solutions[old_ig]
                if len(old_inds) > 0:
                    grid_noiseless.discard(old_inds[0])
                    #print("DISCARD !")
    except Exception as e:
        pass

    if grid_noiseless is not None:
        grid_noiseless.update([ind.to_ind(use_noiseless_fitness=False, use_noiseless_features=True)])
        #grid_noiseless.clear()
        #grid_noiseless.update([i.to_ind(use_noiseless_fitness=False, use_noiseless_features=True) for i in algo.container])

        nb_noiseless = grid_noiseless.size
        if not hasattr(algo.container, 'nb_noiseless'):
            algo.container.nb_noiseless = []
        algo.container.nb_noiseless.append(nb_noiseless)

        #best_fitness_noiseless = grid_noiseless.best_fitness
        best_fitness_noiseless = np.nan
        if algo.container.best is not None:
            best_fitness_noiseless = algo.container.best.to_ind(use_noiseless_fitness=True, use_noiseless_features=True).fitness
        if not hasattr(algo.container, 'best_fitness_noiseless'):
            algo.container.best_fitness_noiseless = []
        algo.container.best_fitness_noiseless.append(best_fitness_noiseless)

        if grid_ref is not None:
            global_reliability = compute_global_reliability(grid_ref, grid_noiseless)
            if not hasattr(algo.container, 'global_reliability'):
                algo.container.global_reliability = []
            algo.container.global_reliability.append(global_reliability)
            #print(f"global_reliability = {global_reliability}")

    nb_samples, tot_nb_samples = compute_nb_samples(algo.container)
    if not hasattr(algo.container, 'nb_samples'):
        algo.container.nb_samples = []
    algo.container.nb_samples.append(nb_samples)
    if not hasattr(algo.container, 'tot_nb_samples'):
        algo.container.tot_nb_samples = []
    algo.container.tot_nb_samples.append(tot_nb_samples)
    #print(f"nb_samples = {nb_samples}")

    sampling_decorator_nb_suggestions = decorator.nb_suggestions
    if not hasattr(algo.container, 'sampling_decorator_nb_suggestions'):
        algo.container.sampling_decorator_nb_suggestions = []
    algo.container.sampling_decorator_nb_suggestions.append(sampling_decorator_nb_suggestions)
    sampling_decorator_nb_evaluations = decorator.nb_evaluations
    if not hasattr(algo.container, 'sampling_decorator_nb_evaluations'):
        algo.container.sampling_decorator_nb_evaluations = []
    algo.container.sampling_decorator_nb_evaluations.append(sampling_decorator_nb_evaluations)



def cleanup_data(a): # Assume minimisation
    a2 = a.copy()
    a2[np.isinf(a2)] = np.nan
    return a2

def normalise(a, min_val, max_val): # Assume minimisation
    a2 = a.copy()
    a2[np.isnan(a2)] = max_val + 1
    a2[a2<min_val] = min_val
    a2[a2>max_val] = np.nan
    return (a2-min_val)/(max_val-min_val)

def compute_nb_samples(grid):
    nb_samples = [ind.nb_samples for ind in grid]
    if len(nb_samples) == 0:
        return 0., 0
    else:
        mean = np.nanmean(nb_samples)
        tot = np.nansum(nb_samples)
        return mean, tot

def compute_global_reliability(grid_ref, grid_test): # Assume minimisation
    if isinstance(grid_ref, containers.Grid):
        base_ref = cleanup_data(grid_ref.quality_array[...,0])
    else:
        base_ref = cleanup_data(grid_ref.to_grid(shape=(64,64)).quality_array[...,0]) # XXX
        #base_ref = cleanup_data(np.array([ind.fitness.values[0] for ind in grid_ref])) # XXX

    if isinstance(grid_test, containers.Grid):
        base_test = cleanup_data(grid_test.quality_array[...,0])
    else:
        base_test = cleanup_data(grid_test.to_grid(shape=(64,64)).quality_array[...,0]) # XXX
        #base_test= cleanup_data(np.array([ind.fitness.values[0] for ind in grid_test])) # XXX

    min_ref = np.nanmin(base_ref)
    max_ref = np.nanmax(base_ref)
    #min_ref = min( np.nanmin(base_ref), np.nanmin(base_test) )
    #max_ref = max( np.nanmax(base_ref), np.nanmax(base_test) )

    normalised_ref = normalise(base_ref, min_ref, max_ref)
    normalised_test = normalise(base_test, min_ref, max_ref)
    mask = ~np.isnan(normalised_ref)
    data_ref = normalised_ref[mask]
    data_test = normalised_test[mask]
    #print(data_ref)
    #print(data_test)
    #sqerrors = np.nan_to_num(1. - np.square(data_ref - data_test, dtype=float))
    #sqerrors[sqerrors < 0.0] = 0.
    ##print(sqerrors)
    #global_reliability = np.sum(sqerrors) / len(data_ref)

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        local_reliability = np.nan_to_num((1.-data_test) / (1.-data_ref))
    local_reliability[local_reliability<0.0] = 0.
    local_reliability[local_reliability>1.0] = 1.
    global_reliability = np.sum(local_reliability) / len(data_ref)

    return global_reliability


def compute_global_reliability_noisy(grid_ref, grid_test): # Assume minimisation
    if isinstance(grid_ref, containers.Grid):
        base_ref = cleanup_data(grid_ref.quality_array[...,0])
    else:
        base_ref = cleanup_data(grid_ref.to_grid(shape=(64,64)).quality_array[...,0]) # XXX
        #base_ref = cleanup_data(np.array([ind.fitness.values[0] for ind in grid_ref])) # XXX

#    if isinstance(grid_test, containers.Grid):
#        base_test = cleanup_data(grid_test.quality_array[...,0])
#    else:
#        base_test = cleanup_data(grid_test.to_grid(shape=(64,64)).quality_array[...,0]) # XXX

    #grid_noiseless = copy.deepcopy(grid_test)
    grid_noiseless = containers.Grid(shape=grid_test.shape, max_items_per_bin=grid_test.max_items_per_bin, fitness_domain=grid_test.fitness_domain, features_domain=grid_test.features_domain)
    nb = grid_noiseless.update([sind.to_ind() for sind in grid_test])
    base_test = cleanup_data(grid_noiseless.quality_array[...,0])

    min_ref = np.nanmin(base_ref)
    max_ref = np.nanmax(base_ref)
    #min_ref = min( np.nanmin(base_ref), np.nanmin(base_test) )
    #max_ref = max( np.nanmax(base_ref), np.nanmax(base_test) )

    normalised_ref = normalise(base_ref, min_ref, max_ref)
    normalised_test = normalise(base_test, min_ref, max_ref)
    mask = ~np.isnan(normalised_ref)
    data_ref = normalised_ref[mask]
    data_test = normalised_test[mask]
    #print(data_ref)
    #print(data_test)
    #sqerrors = np.nan_to_num(1. - np.square(data_ref - data_test, dtype=float))
    #sqerrors[sqerrors < 0.0] = 0.
    ##print(sqerrors)
    #global_reliability = np.sum(sqerrors) / len(data_ref)

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        local_reliability = np.nan_to_num((1.-data_test) / (1.-data_ref))
    local_reliability[local_reliability<0.0] = 0.
    local_reliability[local_reliability>1.0] = 1.
    global_reliability = np.sum(local_reliability) / len(data_ref)

    return global_reliability



# MODELINE "{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
