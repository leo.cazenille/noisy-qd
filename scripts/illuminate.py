#!/usr/bin/env python3

"""An presentation of a simple benchmark for grid-based QD algorithms by using artificial landscapes (i.e. test functions for optimisation tasks). In this benchmark, a test function (e.g. the Rastrigin function) is illuminated with features corresponding to the first two parameters of each genome."""

#from bench_fn import *

from fitness import *
from stats import *
from plots import *
from algos import *

from qdpy import algorithms, containers, benchmarks, plots
from qdpy.experiment import QDExperiment

import numpy as np
import warnings
import os
import random
import yaml
import datetime
import pathlib
import sys
from collections import namedtuple

from bipedal.sim import make_model, simulate


class NoisyQDExperiment(QDExperiment):
    def __init__(self, config_filename, parallelism_type = "concurrent", seed = None, base_config = None, tqdm=True):
        self.tqdm = tqdm
        super().__init__(config_filename, parallelism_type, seed, base_config)


    def _init_bench(self):
        bench_type = self.config.get('bench_type', 'rastrigin')

        self.sigma=1.0
        self.step=0.1
        if bench_type == "rastrigin": #
            self.bench = benchmarks.RastriginBenchmark(nb_features=2)
            self.fitness_domain = ((0., benchmarks.artificial_landscapes.rastrigin([4.5]*2, 10.)[0]),)
        elif bench_type == "normalisedRastrigin":
            self.bench = benchmarks.NormalisedRastriginBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, np.inf),)
        elif bench_type == "sphere":
            self.bench = benchmarks.SphereBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, np.inf),)
        elif bench_type == "eightedSphere":
            self.bench = benchmarks.WeightedSphereBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, np.inf),)
        elif bench_type == "rotatedHyperEllipsoid":
            self.bench = benchmarks.RotatedHyperEllipsoidBenchmark(nb_features=2)
            self.fitness_domain = ((0., 4000.),)
        elif bench_type == "rosenbrock":
            self.bench = benchmarks.RosenbrockBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, np.inf),)
        elif bench_type == "schwefel":
            self.bench = benchmarks.SchwefelBenchmark(nb_features=2)
            self.fitness_domain = ((0., np.inf),)
            self.step=8.0
        elif bench_type == "small_schwefel":
            self.bench = benchmarks.SmallSchwefelBenchmark(nb_features=2)
            self.fitness_domain = ((0., np.inf),)
            self.step=4.0
        elif bench_type == "griewangk":
            self.bench = benchmarks.GriewangkBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, np.inf),)
        elif bench_type == "sumOfPowers": #
            self.bench = benchmarks.SumOfPowersBenchmark(nb_features=2)
            self.fitness_domain = ((0., 2.),)
        elif bench_type == "ackley":
            self.bench = benchmarks.AckleyBenchmark(nb_features=2)
            self.fitness_domain = ((0., 20.),)
        elif bench_type == "styblinskiTang":
            self.bench = benchmarks.StyblinskiTangBenchmark(nb_features=2)
            self.fitness_domain = ((-120., 250.),)
        elif bench_type == "levy":
            self.bench = benchmarks.LevyBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, np.inf),)
        elif bench_type == "perm0db":
            self.bench = benchmarks.Perm0dbBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, np.inf),)
        elif bench_type == "permdb":
            self.bench = benchmarks.PermdbBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, np.inf),)
        elif bench_type == "trid":
            self.bench = benchmarks.TridBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, np.inf),)
        elif bench_type == "zakharov": #
            self.bench = benchmarks.ZakharovBenchmark(nb_features=2)
            self.fitness_domain = ((0., 1000.),)
        elif bench_type == "dixonPrice":
            self.bench = benchmarks.DixonPriceBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, np.inf),)
        elif bench_type == "powell":
            self.bench = benchmarks.PowellBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, np.inf),)
        elif bench_type == "michalewicz":
            self.bench = benchmarks.MichalewiczBenchmark(nb_features=2)
            self.fitness_domain = ((-np.inf, 0.),)
        elif bench_type == "wavy": #
            self.bench = benchmarks.WavyBenchmark(nb_features=2)
            self.fitness_domain = ((0., 2.0),)
        elif bench_type == "trigonometric02":
            self.bench = benchmarks.Trigonometric02Benchmark(nb_features=2)
            self.fitness_domain = ((1., np.inf),)
            self.sigma=200.0
        elif bench_type == "qing":
            self.bench = benchmarks.QingBenchmark(nb_features=2)
            self.fitness_domain = ((0., np.inf),)
            self.sigma=200.0
        elif bench_type == "small_qing":
            self.bench = benchmarks.SmallQingBenchmark(nb_features=2)
            self.fitness_domain = ((0., 500.),)
            self.sigma=0.5
        elif bench_type == "deb01":
            self.bench = benchmarks.Deb01Benchmark(nb_features=2)
            self.fitness_domain = ((-1., 1.),)
            self.sigma=1.0
        elif bench_type == "shubert04":
            self.bench = benchmarks.Shubert04Benchmark(nb_features=2)
            self.fitness_domain = ((-30., 30.),)
            self.sigma=2.0
        else:
            raise f"Unknown benchmark '{bench_type}' !"
        self.features_domain = self.bench.features_domain
        self.ind_domain = self.bench.ind_domain
        self.optimisation_task = self.bench.default_task


    def _init_evalfn(self):
        noise_type = self.config.get('noise_type', 'none')
        beta_fitness = self.config.get('beta_fitness', 0.01)
        beta_features = self.config.get('beta_features', 0.01)
        epsilon = self.config.get('epsilon', 1e-99)
        self.noiseless_eval_fn = self.bench.fn
        if noise_type == "none":
            self.eval_fn = noisify_no_noise(self.noiseless_eval_fn)
        elif noise_type == "fitness_only_gaussian":
            self.eval_fn = noisify_fitness_only_gaussian(self.bench.fn, beta=beta_fitness)
        elif noise_type == "fitness_features_gaussian":
            self.eval_fn = noisify_fitness_features_gaussian(self.bench.fn, beta=beta_fitness, beta_features=beta_features)
        elif noise_type == "fitness_only_uniform":
            self.eval_fn = noisify_fitness_only_uniform(self.bench.fn, ndim=self.algo.dimension, beta=beta_fitness, epsilon=epsilon)
        elif noise_type == "fitness_features_uniform":
            self.eval_fn = noisify_fitness_features_uniform(self.bench.fn, ndim=self.algo.dimension, beta=beta_fitness, beta_features=beta_features, epsilon=epsilon)
        else:
            raise f"Unknown noise type '{noise_type}' !"


    def _init_reference(self):
        reference_file = self.config.get('reference_file', '')
        if len(reference_file) == 0 or reference_file is None:
            self.data_ref = self.grid_ref = None
            return
        reference_file = os.path.expanduser(reference_file)
        with open(reference_file, "rb") as f:
            self.data_ref = pickle.load(f)
        self.grid_ref = self.data_ref['container']


    def reinit(self):
        # Name of the expe instance based on the current timestamp
        self.instance_name = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
        # Identify and create result data dir
        if not self.config.get('dataDir'):
            resultsBaseDir = self.config.get('resultsBaseDir') or "../results/"
            dataDir = os.path.join(os.path.expanduser(resultsBaseDir), os.path.splitext(os.path.basename(self.config_filename))[0])
            self.config['dataDir'] = dataDir
        pathlib.Path(self.config['dataDir']).mkdir(parents=True, exist_ok=True)

        # Init stuffs
        self._init_bench()
        self._init_reference()

        ## Find the domains of the fitness and features
        #self._define_domains()
        default_config = {}
        default_config["fitness_domain"] = self.fitness_domain
        default_config["features_domain"] = self.features_domain
        default_config["optimisation_task"] = self.optimisation_task
        default_config["ind_domain"] = self.ind_domain
        #print(default_config)

        # Create containers and algorithms from configuration
        factory = Factory()
        assert "containers" in self.config, f"Please specify configuration entry 'containers' containing the description of all containers."
        factory.build(self.config["containers"], default_config)
        assert "algorithms" in self.config, f"Please specify configuration entry 'algorithms' containing the description of all algorithms."
        factory.build(self.config["algorithms"], default_config)
        assert "main_algorithm_name" in self.config, f"Please specify configuration entry 'main_algorithm' containing the name of the main algorithm."
        self.algo = factory[self.config["main_algorithm_name"]]
        self.logged_algo = factory[self.config.get('logged_algo', self.config['main_algorithm_name'])]
        self.container = self.algo.container

        self.batch_mode = self.config.get('batch_mode', False)
        self.log_base_path = self.config['dataDir']

        # Add callback
        if self.grid_ref is not None:
            self.grid_noiseless = containers.Grid(shape=self.container.shape, max_items_per_bin=self.container.max_items_per_bin, fitness_domain=self.container.fitness_domain, features_domain=self.container.features_domain)
        else:
            self.grid_noiseless = None
        self.logged_algo.add_callback("tell", algorithms.partial(sampled_tell_callback, grid_ref=self.grid_ref, grid_noiseless=self.grid_noiseless, decorator=self.algo))
            #self.logged_algo.add_callback("iteration", algorithms.partial(sampled_iteration_callback, grid_ref=self.grid_ref))

        # Create a logger to pretty-print everything and generate output data files
        self.iteration_filenames = os.path.join(self.log_base_path, "iteration-%i_" + self.instance_name + ".p")
        self.final_filename = os.path.join(self.log_base_path, "final_" + self.instance_name + ".p")
        self.save_period = self.config.get('save_period', 0)
        logger_class = TQDMAlgorithmLogger if self.tqdm else AlgorithmLogger
        self.logger = logger_class(self.logged_algo,
                iteration_filenames=self.iteration_filenames, final_filename=self.final_filename, save_period=self.save_period)

        # Init evaluation function
        self._init_evalfn()



    def run(self):
        super().run()

        # Create plot QD-score
        # TODO

        # Create plots nb_samples
        plot_path = os.path.join(self.log_base_path, f"nb_samples-{self.instance_name}.pdf")
        plots.plot_evals(self.container.nb_samples, plot_path, "nb_samples", ylim=None)
        print("\nA plot of the number of samples was saved in '%s'." % os.path.abspath(plot_path))

        # Create plots global reliability and stats on noiseless inds
        if self.grid_ref is not None:
            plot_path = os.path.join(self.log_base_path, f"global_reliability-{self.instance_name}.pdf")
            plots.plot_evals(self.container.global_reliability, plot_path, "global_reliability", ylim=(0., 1.))
            print("\nA plot of the global reliability was saved in '%s'." % os.path.abspath(plot_path))
            print(f"Global_reliability[-1]:", self.container.global_reliability[-1])
            plot_path = os.path.join(self.log_base_path, f"nb_noiseless-{self.instance_name}.pdf")
            plots.plot_evals(self.container.nb_noiseless, plot_path, "nb_noiseless", ylim=None)
            print("\nA plot of the number of noiseless was saved in '%s'." % os.path.abspath(plot_path))
            plot_path = os.path.join(self.log_base_path, f"best_fitness_noiseless-{self.instance_name}.pdf")
            plots.plot_evals(self.container.best_fitness_noiseless, plot_path, "best_fitness_noiseless", ylim=None)
            print("\nA plot of the evolution of best fitness noiseless was saved in '%s'." % os.path.abspath(plot_path))




class NoisyBipedalWalkerExperiment(NoisyQDExperiment):
    def _init_bench(self):
        self._define_domains()
        self.fitness_domain = self.config["fitness_domain"]
        self.features_domain = self.config["features_domain"]
        self.optimisation_task = "max"
        self.ind_domain = [-1., 1.]
        Game = namedtuple('Game',
                          ['env_name',
                          'input_size',
                          'output_size',
                          'time_factor',
                          'layers',
                          'activation',
                          'noise_bias',
                          'output_noise',
                          'rnn_mode'])
        game = Game(*self.config['game'].values())
        self.model = make_model(game)

    def _init_evalfn(self):
        pass

    def _init_reference(self):
        self.data_ref = None
        self.grid_ref = None

    def _launchTrial(self, ind):
        self.model.make_env()
        self.model.set_model_params(ind)
        scores = simulate(self.model,
                          render_mode=False,
                          #num_episode=self.config['indv_eps'])
                          num_episode=1)
        fitness = scores[self.fitness_type],
        features = [scores[x] for x in self.features_list]
        return fitness, features



if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--seed', type=int, default=None, help="Numpy random seed")
    parser.add_argument('-p', '--parallelismType', type=str, default='none', help = "Type of parallelism to use (none, concurrent, scoop)")
    parser.add_argument('-o', '--outputDir', type=str, default="results", help = "Path of the output data files")
    #parser.add_argument('--bench', type=str, default="rastrigin", help = "Benchmark function to use")
    parser.add_argument('-c', '--configFilename', type=str, default='conf/test.yaml', help = "Path of configuration file")
    parser.add_argument('--disableTQDM', action='store_true', help="Disable tqdm progress bar")
    parser.add_argument('--saveOutput', action='store_true', help="Save stdout and stderr output to log files")
    args = parser.parse_args()

    #np.seterr(all='raise')

    configFilename = args.configFilename
    config = yaml.safe_load(open(configFilename))
    if len(args.outputDir) > 0:
        config['resultsBaseDir'] = args.outputDir

    # Create experiment
    if "experiment_type" not in config:
        config["experiment_type"] = "artificial_landscapes"
    if config['experiment_type'] == "artificial_landscapes":
        ill = NoisyQDExperiment(args.configFilename, args.parallelismType, seed=args.seed, base_config=config, tqdm=not(args.disableTQDM))
    elif config['experiment_type'] == "bipedal_walker":
        ill = NoisyBipedalWalkerExperiment(args.configFilename, args.parallelismType, seed=args.seed, base_config=config, tqdm=not(args.disableTQDM))
    else:
        raise f"Unknown experiment type '{config['experiment_type']}' !"

    # Redirect stdout and stderr if needed
    if args.saveOutput:
        sys.stdout = open(os.path.join(ill.log_base_path, f"stdout-{ill.instance_name}.log"), 'w')
        sys.stderr = open(os.path.join(ill.log_base_path, f"stderr-{ill.instance_name}.log"), 'w')

    #print("Seed: %i" % seed)
    print("Using configuration file '%s'. Instance name: '%s'" % (args.configFilename, ill.instance_name))
    ill.run()


# MODELINE "{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
