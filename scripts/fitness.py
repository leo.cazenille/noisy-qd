#!/usr/bin/env python3


from qdpy import algorithms, containers, benchmarks, plots

import numpy as np
import random
import warnings


def exec_multiple(fn):
    def fn2(inds):
        return [fn(ind) for ind in inds]
    return fn2

#def noisify_fitness_only_gaussian(fn, sigma = 0.625):
#    def noisy_fn(ind):
#        fitness, features = fn(ind)
#        fit_vals = np.array(fitness)
#        fit_vals += np.random.normal(0., sigma)
#        fitness = tuple(fit_vals)
#        return fitness, features
#    return noisy_fn
#
#def noisify_fitness_features_gaussian(fn, sigma = 0.625, sigma_features = 0.01):
#    def noisy_fn(ind):
#        fitness, features = fn(ind)
#        fit_vals = np.array(fitness)
#        fit_vals += np.random.normal(0., sigma)
#        fitness = tuple(fit_vals)
#        features_vals = np.array(features)
#        features_vals += np.random.normal(0., sigma_features)
#        features = list(features_vals)
#        return fitness, features
#    return noisy_fn


def noisify_no_noise(fn):
    def noisy_fn(ind):
        fitness, features = fn(ind)
        ind.noiseless_fitness = fitness
        ind.noiseless_features = features
        return fitness, features
    return noisy_fn

def noisify_fitness_only_gaussian(fn, beta = 0.01):
    def noisy_fn(ind):
        fitness, features = fn(ind)
        ind.noiseless_fitness = fitness
        ind.noiseless_features = features
        fit_vals = np.array(fitness)
        fit_vals *= np.exp(beta * np.random.normal(0., 1., size=fit_vals.shape))
        fitness = tuple(fit_vals)
        return fitness, features
    return noisy_fn

def noisify_fitness_features_gaussian(fn, beta = 0.01, beta_features = 0.01):
    def noisy_fn(ind):
        fitness, features = fn(ind)
        ind.noiseless_fitness = fitness
        ind.noiseless_features = features
        fit_vals = np.array(fitness)
        fit_vals *= np.exp(beta * np.random.normal(0., 1., size=fit_vals.shape))
        fitness = tuple(fit_vals)
        features_vals = np.array(features)
        features_vals *= np.exp(beta_features * np.random.normal(0., 1., size=features_vals.shape))
        features = list(features_vals)
        return fitness, features
    return noisy_fn


def noisify_fitness_only_uniform(fn, ndim = 2, beta = 0.01, epsilon = 1e-99):
    alpha = beta * (0.49 + 1. / ndim)
    def noisy_fn(ind):
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore', category=RuntimeWarning)
            fitness, features = fn(ind)
            ind.noiseless_fitness = fitness
            ind.noiseless_features = features
            fit_vals = np.array(fitness)
            n = fit_vals.shape
            fit_vals *= np.random.random(size=n)**beta * np.maximum(np.ones(n), np.power(1e9/(np.abs(fit_vals)+epsilon), alpha * np.random.random(size=n)))
            fitness = tuple(fit_vals)
        return fitness, features
    return noisy_fn

def noisify_fitness_features_uniform(fn, ndim = 2, beta = 0.01, beta_features = 0.01, epsilon = 1e-99):
    alpha = beta * (0.49 + 1. / ndim)
    alpha_features = beta_features * (0.49 + 1. / ndim)
    def noisy_fn(ind):
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore', category=RuntimeWarning)
            fitness, features = fn(ind)
            ind.noiseless_fitness = fitness
            ind.noiseless_features = features
            fit_vals = np.array(fitness)
            n = fit_vals.shape
            fit_vals *= np.random.random(size=n)**beta * np.maximum(np.ones(n), np.power(1e9/(np.abs(fit_vals)+epsilon), alpha * np.random.random(size=n)))
            fitness = tuple(fit_vals)
            features_vals = np.array(features)
            m = features_vals.shape
            features_vals *= np.random.random(size=m)**beta_features * np.maximum(np.ones(m), np.power(1e9/(np.abs(features_vals)+epsilon), alpha_features * np.random.random(size=m)))
            features = list(features_vals)
        return fitness, features
    return noisy_fn




## TODO
#def noisify_fitness_only_cauchy(fn, ndim = 2, alpha = 0.01, p = 0.05, epsilon = 1e-199):
#    def noisy_fn(ind):
#        with warnings.catch_warnings():
#            warnings.filterwarnings('ignore', category=RuntimeWarning)
#            fitness, features = fn(ind)
#            ind.noiseless_fitness = fitness
#            ind.noiseless_features = features
#            fit_vals = np.array(fitness)
#            n = fit_vals.shape
#            #fit_vals *= np.random.random(size=n)**beta * np.maximum(np.ones(n), np.power(1e9/(fit_vals+epsilon), alpha * np.random.random(size=n)))
#            fit_vals += alpha * np.maximum(np.zeros(n), 1000. + (np.random.random(size=n) < 0.05) * (np.random.normal(size=n) / np.abs(np.random.normal(size=n) + epsilon))
#            fitness = tuple(fit_vals)
#        return fitness, features
#    return noisy_fn
#
## TODO
#def noisify_fitness_features_cauchy(fn, ndim = 2, alpha = 0.01, alpha_features = 0.01, p = 0.05, epsilon = 1e-199):
#    alpha = beta * (0.49 + 1. / ndim)
#    alpha_features = beta_features * (0.49 + 1. / ndim)
#    def noisy_fn(ind):
#        with warnings.catch_warnings():
#            warnings.filterwarnings('ignore', category=RuntimeWarning)
#            fitness, features = fn(ind)
#            ind.noiseless_fitness = fitness
#            ind.noiseless_features = features
#            fit_vals = np.array(fitness)
#            n = fit_vals.shape
#            fit_vals *= np.random.random(size=n)**beta * np.maximum(np.ones(n), np.power(1e9/(fit_vals+epsilon), alpha * np.random.random(size=n)))
#            fitness = tuple(fit_vals)
#            features_vals = np.array(features)
#            m = features_vals.shape
#            features_vals *= np.random.random(size=m)**beta_features * np.maximum(np.ones(m), np.power(1e9/(features_vals+epsilon), alpha_features * np.random.random(size=m)))
#            features = list(features_vals)
#        return fitness, features
#    return noisy_fn


# MODELINE "{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
