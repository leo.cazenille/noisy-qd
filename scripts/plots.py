#!/usr/bin/env python3


import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from cycler import cycler
from matplotlib.ticker import FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D

from qdpy import algorithms, containers, benchmarks, plots

import numpy as np
import warnings
import os
import random
from scipy.constants import golden_ratio

#from algos import *


from collections import OrderedDict
_linestyles = OrderedDict(
    [('solid',               (0, ())),
     #('loosely dotted',      (0, (1, 10))),
     ('dotted',              (0, (1, 5))),
     ('densely dotted',      (0, (1, 1))),

     #('loosely dashed',      (0, (5, 10))),
     ('dashed',              (0, (5, 5))),
     ('densely dashed',      (0, (5, 1))),

     #('loosely dashdotted',  (0, (3, 10, 1, 10))),
     #('dashdotted',          (0, (3, 5, 1, 5))),
     ('densely dashdotted',  (0, (3, 1, 1, 1))),

     #('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))])


def plot_combined_global_reliability(algo_ref, algos, output_filename="global_reliability.pdf", figsize=(4.*golden_ratio,4.)):
    assert(len(algos))
    data_tests = [a.container.global_reliability for a in algos]

    fig, ax = plt.subplots(figsize=figsize)
    x = np.arange(len(data_tests[0]))

    ##linestyle_cycler = cycler('linestyle',['-','--',':','-.','-','--',':']) + cycler(color=plt.get_cmap("Set2",8).colors)
    ##linestyle_cycler = cycler('linestyle', list(_linestyles.values())[:8]) + cycler(color=plt.get_cmap("Dark2",8).colors)
    #linestyle_cycler = cycler('linestyle', list(_linestyles.values())[:8]) + cycler(color=['r', 'r', 'r', 'r', 'g', 'g', 'g', 'g'])
    #linestyle_cycler = cycler('linestyle', list(_linestyles.values())[:4] * 2) + cycler(color=['r', 'g', 'r', 'g', 'r', 'g', 'r', 'g'])
    #linestyle_cycler = cycler('linestyle',['-','-','-','-',':',':',':',':']) + cycler(color=["#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#e41a1c", "#377eb8", "#4daf4a", "#984ea3"])
    linestyle_cycler = cycler('linestyle',['-','-','-','-',':',':',':',':']) + cycler(color=["#e66101", "#fdb863", "#b2abd2", "#5e3c99", "#e66101", "#fdb863", "#b2abd2", "#5e3c99"])
    ax.set_prop_cycle(linestyle_cycler)
    plt.xticks(rotation=20)
    for d, a in zip(data_tests, algos):
        ax.plot(x, d, label=a.name, linewidth=3)

    ax.set_ylim((0., 1.))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.0E'))
    plt.xlabel("Evaluations", fontsize=20)
    plt.ylabel("Global reliability", fontsize=20)
    for t in ax.xaxis.get_major_ticks(): t.label.set_fontsize(19)
    for t in ax.yaxis.get_major_ticks(): t.label.set_fontsize(19)

    #plt.tight_layout()
    #plt.legend(title="Dimension", loc="lower right", fontsize=12, title_fontsize=14)
    #box = ax.get_position()
    #ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    plt.legend(loc="center left", fontsize=16, title_fontsize=16, bbox_to_anchor=(1.04, 0.5), borderaxespad=0)

    fig.savefig(output_filename, bbox_inches="tight")
    plt.close(fig)


def plot3D(bench, output_filename="plot3D.pdf", step=0.1):
    def fn_arg0(ind):
        return bench.fn(ind)[0][0]

    fig = plt.figure(figsize=(4.*golden_ratio,4.))
    ax = fig.add_subplot(111, projection='3d', azim=-19, elev=30, position=[0.25, 0.15, 0.7, 0.7]) 
    X = np.arange(bench.ind_domain[0], bench.ind_domain[1], step)
    Y = np.arange(bench.ind_domain[0], bench.ind_domain[1], step)
    X, Y = np.meshgrid(X, Y)
    Z = np.fromiter(map(fn_arg0, zip(X.flat,Y.flat)), dtype=np.float, count=X.shape[0]*X.shape[1]).reshape(X.shape)
    ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=plt.get_cmap("inferno_r"), linewidth=0.2)

    ax.set_xlabel("x0", fontsize=14)
    ax.set_ylabel("x1", fontsize=14)
    #ax.set_xlabel("Feature 1", fontsize=14)
    #ax.set_ylabel("Feature 2", fontsize=14)
    ax.set_zlabel("Fitness", fontsize=14)
    # change fontsize
    for t in ax.xaxis.get_major_ticks(): t.label.set_fontsize(14)
    for t in ax.yaxis.get_major_ticks(): t.label.set_fontsize(14)
    for t in ax.zaxis.get_major_ticks(): t.label.set_fontsize(14)
    plt.tight_layout()
    #fig.subplots_adjust(right=0.85, bottom=0.10, wspace=0.10)
    fig.savefig(output_filename)
    plt.close(fig)




# MODELINE "{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
