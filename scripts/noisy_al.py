#!/usr/bin/env python3

"""An presentation of a simple benchmark for grid-based QD algorithms by using artificial landscapes (i.e. test functions for optimisation tasks). In this benchmark, a test function (e.g. the Rastrigin function) is illuminated with features corresponding to the first two parameters of each genome."""

#from bench_fn import *

from fitness import *
from stats import *
from plots import *
from algos import *

from qdpy import algorithms, containers, benchmarks, plots

import numpy as np
import warnings
import os
import random



def compute_ref(bench, budget=60000, dimension=2, nb_bins_per_feature=64, output_path="ref", algo_name=None, fitness_domain=((0., 120.),)):
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    # Create grid and algorithm
    grid_ref = containers.Grid(shape=(nb_bins_per_feature,) * bench.nb_features, max_items_per_bin=1,
            fitness_domain=bench.fitness_domain, features_domain=bench.features_domain)
    algo_ref = algorithms.RandomSearchMutPolyBounded(grid_ref, budget=budget, batch_size=500,
            dimension=dimension, optimisation_task=bench.default_task, ind_domain=bench.ind_domain,
            name=algo_name)

    # Create a logger to pretty-print everything and generate output data files
    logger_ref = algorithms.TQDMAlgorithmLogger(algo_ref, log_base_path=output_path)
    # Define evaluation function
    eval_fn = bench.fn
    # Run illumination process !
    best = algo_ref.optimise(eval_fn)
    # Print results info
    #print(algo_ref.summary())
    # Plot the results
    plots.default_plots_grid(logger_ref, to_grid_parameters={'shape': (nb_bins_per_feature,) * bench.nb_features}, fitness_domain=fitness_domain)
    return algo_ref, logger_ref


#def compute_test(bench, algo_ref, dimension=3, output_path="test", algo_name=None, mut_pb=0.5, eta=20., fitness_domain=((0., 120.),)):
#    if not os.path.exists(output_path):
#        os.makedirs(output_path)
#
#    # Create container and algorithm
#    grid_test = containers.Grid(shape=(algo_ref.container.shape[0],) * bench.nb_features, max_items_per_bin=1,
#            fitness_domain=bench.fitness_domain, features_domain=bench.features_domain)
#    #grid_test = containers.NoveltyArchive(k=1, threshold_novelty=0.016, fitness_domain=bench.fitness_domain, features_domain=bench.features_domain, storage_type=list, depot_type=list)
#    grid_test.global_reliability = []
#    #algo_test = algorithms.RandomSearchMutPolyBounded(grid_test, budget=algo_ref.budget, batch_size=500,
#    #        dimension=dimension, optimisation_task=bench.default_task, ind_domain=bench.ind_domain,
#    #        name=algo_name)
#    algo_test = algorithms.MutPolyBounded(grid_test, budget=algo_ref.budget, batch_size=500,
#            dimension=dimension, optimisation_task=bench.default_task, ind_domain=bench.ind_domain,
#            #sel_pb = 1.0, init_pb = 0.0, mut_pb = 0.8, eta = 20., name=algo_name)
#            mut_pb = mut_pb, eta = eta, name=algo_name)
#
#    #grid_surrogate = containers.Grid(shape=(algo_ref.container.shape[0],) * bench.nb_features, max_items_per_bin=1,
#    #        fitness_domain=bench.fitness_domain, features_domain=bench.features_domain)
#    #grid_test = containers.Grid(shape=(algo_ref.container.shape[0],) * bench.nb_features, max_items_per_bin=1,
#    #        fitness_domain=bench.fitness_domain, features_domain=bench.features_domain)
#    #grid_test.global_reliability = []
#    #algo_surrogate = algorithms.RandomSearchMutPolyBounded(grid_surrogate, budget=5000, batch_size=500,
#    #        dimension=dimension, optimisation_task=bench.default_task, ind_domain=bench.ind_domain)
#    #algo_illumination = algorithms.RandomSearchMutPolyBounded(grid_test, budget=algo_ref.budget, batch_size=500,
#    #        dimension=dimension, optimisation_task=bench.default_task, ind_domain=bench.ind_domain)
#    #algo_test = algorithms.SAIL(illumination_algo=algo_illumination, acquisition_algo=algo_surrogate, name=algo_name)
#
#
#
#    algo_test.add_callback("tell", algorithms.partial(tell_callback, grid_ref=algo_ref.container))
#    #algo_test.add_callback("iteration", algorithms.partial(iteration_callback, grid_ref=algo_ref.container))
#
#    # Create a logger to pretty-print everything and generate output data files
#    logger_test = algorithms.TQDMAlgorithmLogger(algo_test, log_base_path=output_path)
#    # Define evaluation function
#    eval_fn = bench.fn
#    # Run illumination process !
#    best = algo_test.optimise(eval_fn)
#    # Print results info
#    #print(algo_test.summary())
#    # Plot the results
#    plots.default_plots_grid(logger_test, to_grid_parameters={'shape': algo_ref.container.shape}, fitness_domain=fitness_domain)
#    # Plot global_reliability per eval
#    #global_reliability = compute_global_reliability(grid_ref, grid_test)
#    #print(f"global_reliability = {global_reliability}")
#    plots.plot_evals(grid_test.global_reliability, os.path.join(logger_test.log_base_path, "global_reliability.pdf"), "global_reliability", ylim=(0., 1.))
#    print(f"dimension={dimension}  global_reliability[-1]:", grid_test.global_reliability[-1])
#    return algo_test, logger_test
#
#
#
#def compute_test2(bench, algo_ref, dimension=3, output_path="test", algo_name=None, mut_pb=0.5, mu=0., sigma=1.0, fitness_domain=((0., 120.),)):
#    if not os.path.exists(output_path):
#        os.makedirs(output_path)
#
#    # Create container and algorithm
#    grid_test = containers.Grid(shape=(algo_ref.container.shape[0],) * bench.nb_features, max_items_per_bin=1,
#            fitness_domain=bench.fitness_domain, features_domain=bench.features_domain)
#    grid_test.global_reliability = []
#    algo_test = algorithms.MutGaussian(grid_test, budget=algo_ref.budget, batch_size=500,
#            dimension=dimension, optimisation_task=bench.default_task, ind_domain=bench.ind_domain,
#            mut_pb = mut_pb, mu = mu, sigma=sigma, name=algo_name)
#
#    algo_test.add_callback("tell", algorithms.partial(tell_callback, grid_ref=algo_ref.container))
#    #algo_test.add_callback("iteration", algorithms.partial(iteration_callback, grid_ref=algo_ref.container))
#
#    # Create a logger to pretty-print everything and generate output data files
#    logger_test = algorithms.TQDMAlgorithmLogger(algo_test, log_base_path=output_path)
#    # Define evaluation function
#    eval_fn = bench.fn
#    # Run illumination process !
#    best = algo_test.optimise(eval_fn)
#    # Print results info
#    #print(algo_test.summary())
#    # Plot the results
#    plots.default_plots_grid(logger_test, to_grid_parameters={'shape': algo_ref.container.shape}, fitness_domain=fitness_domain)
#    # Plot global_reliability per eval
#    plots.plot_evals(grid_test.global_reliability, os.path.join(logger_test.log_base_path, "global_reliability.pdf"), "global_reliability", ylim=(0., 1.))
#    print(f"dimension={dimension}  global_reliability[-1]:", grid_test.global_reliability[-1])
#    return algo_test, logger_test



def compute_noisy(bench, algo_ref, dimension=3, output_path="test", algo_name=None, mut_pb=1.0, eta=20., fitness_domain=((0., 120.),), parallelism_type="none"):
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    # Create container and algorithm
    grid_test = containers.Grid(shape=(algo_ref.container.shape[0],) * bench.nb_features, max_items_per_bin=1,
            fitness_domain=bench.fitness_domain, features_domain=bench.features_domain)
    grid_test.global_reliability = []
    grid_test.nb_samples = []
    algo_test = MutPolyBoundedForNoisyInds(grid_test, budget=algo_ref.budget * 10, batch_size=500,
            dimension=dimension, optimisation_task=bench.default_task, ind_domain=bench.ind_domain,
            mut_pb = mut_pb, eta = eta, name=algo_name, base_ind_gen=gen_sampled_individuals())

    #algo_test.add_callback("tell", algorithms.partial(tell_callback, grid_ref=algo_ref.container))
    algo_test.add_callback("tell", algorithms.partial(sampled_tell_callback, grid_ref=algo_ref.container))
    #algo_test.add_callback("iteration", algorithms.partial(iteration_callback, grid_ref=algo_ref.container))

    #algo_adaptive_sampling = FixedSamplingDecorator(algo_test, nb_samples = 10, batch_size=500)
    algo_adaptive_sampling = FixedSamplingDecorator(algo_test, nb_samples = 10, batch_size=500)
#    algo_adaptive_sampling = AdaptiveSamplingDecorator(algo_test, batch_size=500,
#            dimension=dimension, optimisation_task=bench.default_task, ind_domain=bench.ind_domain,
#            mut_pb = mut_pb, eta = eta, name=algo_name, base_ind_gen=gen_sampled_individuals(), batch_mode=True)
##    algo_adaptive_sampling.add_callback("tell", algorithms.partial(tell_callback, grid_ref=algo_ref.container))
##    #algo_adaptive_sampling.add_callback("iteration", algorithms.partial(iteration_callback, grid_ref=algo_ref.container))

    # Create a logger to pretty-print everything and generate output data files
    logger_test = algorithms.TQDMAlgorithmLogger(algo_test, log_base_path=output_path)
    #logger_test = algorithms.TQDMAlgorithmLogger(algo_adaptive_sampling, log_base_path=output_path)
    # Define evaluation function
    #eval_fn = noisify_fitness_only_gaussian(bench.fn, sigma=0.625)
    eval_fn = noisify_fitness_features_gaussian(bench.fn, beta=0.01, beta_features=0.01)
    #eval_fn = noisify_fitness_features_gaussian(rastrigin_2features, sigma=0.625, sigma_features=0.01)
    #eval_fn = exec_multiple(eval_fn)
    #eval_fn = rastrigin_2features_
    # Run illumination process !
    with ParallelismManager(parallelism_type) as pMgr:
##        best = algo_test.optimise(eval_fn, executor=pMgr.executor)
        best = algo_adaptive_sampling.optimise(eval_fn, executor=pMgr.executor, batch_mode = False)
        #best = algo_adaptive_sampling.optimise(eval_fn, executor=pMgr.executor, send_several_suggestions_to_fn=True)
    # Print results info
    #print(algo_test.summary())
    # Plot the results
    plots.default_plots_grid(logger_test, to_grid_parameters={'shape': algo_ref.container.shape}, fitness_domain=fitness_domain)
    # Plot global_reliability per eval
    #global_reliability = compute_global_reliability(grid_ref, grid_test)
    #print(f"global_reliability = {global_reliability}")
    plots.plot_evals(grid_test.global_reliability, os.path.join(logger_test.log_base_path, "global_reliability.pdf"), "global_reliability", ylim=(0., 1.))
    plots.plot_evals(grid_test.nb_samples, os.path.join(logger_test.log_base_path, "nb_samples.pdf"), "nb_samples", ylim=None)
    print(f"dimension={dimension}  global_reliability[-1]:", grid_test.global_reliability[-1])
    #return algo_test, logger_test
    return algo_adaptive_sampling, logger_test



if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--seed', type=int, default=None, help="Numpy random seed")
    parser.add_argument('-p', '--parallelismType', type=str, default='none', help = "Type of parallelism to use (none, concurrent, scoop)")
    parser.add_argument('-o', '--outputDir', type=str, default="results", help = "Path of the output log files")
    parser.add_argument('--bench', type=str, default="rastrigin", help = "Benchmark function to use")
    args = parser.parse_args()

    # Find random seed
    if args.seed is not None:
        seed = args.seed
    else:
        seed = np.random.randint(1000000)

    # Update and print seed
    np.random.seed(seed)
    random.seed(seed)
    print("Seed: %i" % seed)

    # Find where to put logs
    log_base_path = args.outputDir

    # Create container and algorithm. Here we use MAP-Elites, by illuminating a Grid container by evolution.
    sigma=1.0
    step=0.1
    if args.bench == "rastrigin": #
        bench = benchmarks.RastriginBenchmark(nb_features=2)
        fitness_domain = ((0., 120.),)
    elif args.bench == "normalisedRastrigin":
        bench = benchmarks.NormalisedRastriginBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, np.inf),)
    elif args.bench == "sphere":
        bench = benchmarks.SphereBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, np.inf),)
    elif args.bench == "eightedSphere":
        bench = benchmarks.WeightedSphereBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, np.inf),)
    elif args.bench == "rotatedHyperEllipsoid":
        bench = benchmarks.RotatedHyperEllipsoidBenchmark(nb_features=2)
        fitness_domain = ((0., 4000.),)
    elif args.bench == "rosenbrock":
        bench = benchmarks.RosenbrockBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, np.inf),)
    elif args.bench == "schwefel":
        bench = benchmarks.SchwefelBenchmark(nb_features=2)
        fitness_domain = ((0., np.inf),)
        step=8.0
    elif args.bench == "small_schwefel":
        bench = benchmarks.SmallSchwefelBenchmark(nb_features=2)
        fitness_domain = ((0., np.inf),)
        step=4.0
    elif args.bench == "griewangk":
        bench = benchmarks.GriewangkBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, np.inf),)
    elif args.bench == "sumOfPowers": #
        bench = benchmarks.SumOfPowersBenchmark(nb_features=2)
        fitness_domain = ((0., 2.),)
    elif args.bench == "ackley":
        bench = benchmarks.AckleyBenchmark(nb_features=2)
        fitness_domain = ((0., 20.),)
    elif args.bench == "styblinskiTang":
        bench = benchmarks.StyblinskiTangBenchmark(nb_features=2)
        fitness_domain = ((-120., 250.),)
    elif args.bench == "levy":
        bench = benchmarks.LevyBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, np.inf),)
    elif args.bench == "perm0db":
        bench = benchmarks.Perm0dbBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, np.inf),)
    elif args.bench == "permdb":
        bench = benchmarks.PermdbBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, np.inf),)
    elif args.bench == "trid":
        bench = benchmarks.TridBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, np.inf),)
    elif args.bench == "zakharov": #
        bench = benchmarks.ZakharovBenchmark(nb_features=2)
        fitness_domain = ((0., 1000.),)
    elif args.bench == "dixonPrice":
        bench = benchmarks.DixonPriceBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, np.inf),)
    elif args.bench == "powell":
        bench = benchmarks.PowellBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, np.inf),)
    elif args.bench == "michalewicz":
        bench = benchmarks.MichalewiczBenchmark(nb_features=2)
        fitness_domain = ((-np.inf, 0.),)
    elif args.bench == "wavy": #
        bench = benchmarks.WavyBenchmark(nb_features=2)
        fitness_domain = ((0., 2.0),)
    elif args.bench == "trigonometric02":
        bench = benchmarks.Trigonometric02Benchmark(nb_features=2)
        fitness_domain = ((1., np.inf),)
        sigma=200.0
    elif args.bench == "qing":
        bench = benchmarks.QingBenchmark(nb_features=2)
        fitness_domain = ((0., np.inf),)
        sigma=200.0
    elif args.bench == "small_qing":
        bench = benchmarks.SmallQingBenchmark(nb_features=2)
        fitness_domain = ((0., 500.),)
        sigma=0.5
    elif args.bench == "deb01":
        bench = benchmarks.Deb01Benchmark(nb_features=2)
        fitness_domain = ((-1., 1.),)
        sigma=1.0
    elif args.bench == "shubert04":
        bench = benchmarks.Shubert04Benchmark(nb_features=2)
        fitness_domain = ((-30., 30.),)
        sigma=2.0
    else:
        raise f"Unknown benchmark '{args.bench}' !"

    #fitness_domain = ((0., np.inf),)

#    # Plot 3D
#    plot3D(bench, output_filename=os.path.join(log_base_path, "plot3D.pdf"), step=step)

    # Compute reference
    algo_name_ref = args.bench + "-ref"
    #algo_ref, logger_ref = compute_ref(bench, budget=1000000, dimension=2, nb_bins_per_feature=64,
    #algo_ref, logger_ref = compute_ref(bench, budget=100000, dimension=2, nb_bins_per_feature=64,
    #algo_ref, logger_ref = compute_ref(bench, budget=100000, dimension=2, nb_bins_per_feature=16,
    #algo_ref, logger_ref = compute_ref(bench, budget=10000, dimension=2, nb_bins_per_feature=32,
    algo_ref, logger_ref = compute_ref(bench, budget=1000, dimension=2, nb_bins_per_feature=16,
    #algo_ref, logger_ref = compute_ref(bench, budget=1000, dimension=2, nb_bins_per_feature=64,
            output_path=os.path.join(log_base_path, algo_name_ref), algo_name=algo_name_ref, fitness_domain=fitness_domain)

    # Compute benchmark for several dimensions
    #tested_dim = [3, 4, 6, 8, 10, 14]
    #tested_dim = [3, 6, 10, 14]
    tested_dim = [2]
    algos = []
    loggers = []
#    for dim in tested_dim:
#        #algo_name = f"Dimension {dim}"
#        algo_name = f"ME1 {dim} dim"
#        a,l = compute_test(bench, algo_ref, dimension=dim,
#                output_path=os.path.join(log_base_path, algo_name), algo_name=algo_name, fitness_domain=fitness_domain)
#        algos.append(a)
#        loggers.append(l)
#        #print(a.summary())
#
#    for dim in tested_dim:
#        algo_name = f"ME2 {dim} dim"
#        a,l = compute_test2(bench, algo_ref, dimension=dim, sigma=sigma,
#                output_path=os.path.join(log_base_path, algo_name), algo_name=algo_name, fitness_domain=fitness_domain)
#        algos.append(a)
#        loggers.append(l)
#        #print(a.summary())

    for dim in tested_dim:
        algo_name = f"ME1 {dim} dim"
        a,l = compute_noisy(bench, algo_ref, dimension=dim,
                output_path=os.path.join(log_base_path, algo_name), algo_name=algo_name, fitness_domain=fitness_domain,
                parallelism_type=args.parallelismType)
        algos.append(a)
        loggers.append(l)
        print(a.summary())

    # Make combined plots
    plot_combined_global_reliability(algo_ref, algos, output_filename=os.path.join(log_base_path, "global_reliability.pdf"))


# MODELINE "{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
