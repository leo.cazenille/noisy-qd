#    This file is part of qdpy.
#
#    qdpy is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    qdpy is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with qdpy. If not, see <http://www.gnu.org/licenses/>.

# TODO Say from ... + cite


"""
import
"""

import numpy as np
import random
import json
import sys
#import config
import time
#from gym.wrappers import Monitor
#import gym
#import pybullet as p
#import pybullet_envs


"""
NN functions
"""
def sigmoid(x):
  return 1 / (1 + np.exp(-x))

def relu(x):
  return np.maximum(x, 0)

def passthru(x):
  return x

# useful for discrete actions
def softmax(x):
  e_x = np.exp(x - np.max(x))
  return e_x / e_x.sum(axis=0)

# useful for discrete actions
def sample(p):
  return np.argmax(np.random.multinomial(1, p))


"""
functions
"""
def make_model(game):
  model = Model(game)
  return model

def make_env(env_name, seed=-1, render_mode=False):
  if (env_name.startswith("BipedalWalker")):
    if (env_name.startswith("BipedalWalkerHardcore")):
      from bipedal.biped import BipedalWalkerHardcore
      env = BipedalWalkerHardcore()
    else:
      from bipedal.biped import BipedalWalker
      env = BipedalWalker()

  if (seed >= 0):
    env.seed(seed)

  '''
  print("environment details")
  print("env.action_space", env.action_space)
  print("high, low", env.action_space.high, env.action_space.low)
  print("environment details")
  print("env.observation_space", env.observation_space)
  print("high, low", env.observation_space.high, env.observation_space.low)
  assert False
  '''
  return env




"""
model
"""
class Model:
  ''' simple feedforward model '''
  def __init__(self, game):
    self.output_noise = game.output_noise
    self.env_name = game.env_name
    self.layer_1 = game.layers[0]
    self.layer_2 = game.layers[1]
    self.time_input = 0               # use extra sinusoid input
    self.sigma_bias = game.noise_bias # bias in stdev of output
    self.sigma_factor = 0.5           # multiplicative in stdev of output
    if game.time_factor > 0:
      self.time_factor = float(game.time_factor)
      self.time_input = 1
    self.input_size = game.input_size
    self.output_size = game.output_size
    if self.layer_2 > 0:
      self.shapes = [ (self.input_size + self.time_input, self.layer_1),
                      (self.layer_1, self.layer_2),
                      (self.layer_2, self.output_size)]
    elif self.layer_2 == 0:
      self.shapes = [ (self.input_size + self.time_input, self.layer_1),
                      (self.layer_1, self.output_size)]
    else:
      assert False, "invalid layer_2"

    self.sample_output = False

    # activation type
    if game.activation == 'relu':
      self.activations = [relu, relu, passthru]
    elif game.activation == 'sigmoid':
      self.activations = [np.tanh, np.tanh, sigmoid]
    elif game.activation == 'softmax':
      self.activations = [np.tanh, np.tanh, softmax]
      self.sample_output = True
    elif game.activation == 'passthru':
      self.activations = [np.tanh, np.tanh, passthru]
    else:
      self.activations = [np.tanh, np.tanh, np.tanh]

    self.weight = []
    self.bias = []
    self.bias_log_std = []
    self.bias_std = []
    self.param_count = 0

    idx = 0
    for shape in self.shapes:
      self.weight.append(np.zeros(shape=shape))
      self.bias.append(np.zeros(shape=shape[1]))
      self.param_count += (np.product(shape) + shape[1])
      if self.output_noise[idx]:
        self.param_count += shape[1]
      log_std = np.zeros(shape=shape[1])
      self.bias_log_std.append(log_std)
      out_std = np.exp(self.sigma_factor*log_std + self.sigma_bias)
      self.bias_std.append(out_std)
      idx += 1

    self.render_mode = False

  def make_env(self, seed=-1, render_mode=False):
    self.render_mode = render_mode
    self.env = make_env(self.env_name, seed=seed, render_mode=render_mode)

  def get_action(self, x, t=0, mean_mode=False):
    # if mean_mode = True, ignore sampling.
    h = np.array(x).flatten()
    if self.time_input == 1:
      time_signal = float(t) / self.time_factor
      h = np.concatenate([h, [time_signal]])
    num_layers = len(self.weight)
    for i in range(num_layers):
      w = self.weight[i]
      b = self.bias[i]
      h = np.matmul(h, w) + b
      if (self.output_noise[i] and (not mean_mode)):
        out_size = self.shapes[i][1]
        out_std = self.bias_std[i]
        output_noise = np.random.randn(out_size)*out_std
        h += output_noise
      h = self.activations[i](h)

    if self.sample_output:
      h = sample(h)

    return h

  def set_model_params(self, model_params):
    pointer = 0
    for i in range(len(self.shapes)):
      w_shape = self.shapes[i]
      b_shape = self.shapes[i][1]
      s_w = np.product(w_shape)
      s = s_w + b_shape
      chunk = np.array(model_params[pointer:pointer+s])
      self.weight[i] = chunk[:s_w].reshape(w_shape)
      self.bias[i] = chunk[s_w:].reshape(b_shape)
      pointer += s
      if self.output_noise[i]:
        s = b_shape
        self.bias_log_std[i] = np.array(model_params[pointer:pointer+s])
        self.bias_std[i] = np.exp(self.sigma_factor*self.bias_log_std[i] + self.sigma_bias)
        if self.render_mode:
          print("bias_std, layer", i, self.bias_std[i])
        pointer += s

  def load_model(self, filename):
    with open(filename) as f:    
      data = json.load(f)
    print('loading file %s' % (filename))
    self.data = data
    model_params = np.array(data[0]) # assuming other stuff is in data
    self.set_model_params(model_params)

  def get_random_model_params(self, stdev=0.1):
    return np.random.randn(self.param_count)*stdev



def simulate(model, render_mode=True, num_episode=5, seed=-1, max_len=-1):
  reward_list = []
  t_list = []

  max_episode_length = 3000

#  if (seed >= 0):
#    random.seed(seed)
#    np.random.seed(seed)
#    model.env.seed(seed)

  episodes_reward_sum = 0
  episodes_feature_sum = (0,0,0,0, 0,0,0,0, 0,0,0,0)

  total_reward = 0.0
  total_features = (0,0,0,0, 0,0,0,0, 0,0,0,0)

  for episode in range(num_episode):
    obs = model.env.reset()

    if obs is None:
      obs = np.zeros(model.input_size)
    
    for t in range(max_episode_length):
      if render_mode:
        model.env.render("human")

      #action = model.get_action(obs, t=t, mean_mode=False)
      action = model.get_action(obs)

      prev_obs = obs

      obs, reward, features, done, info = model.env.step(action)

      if (render_mode):
        pass
        print("action", action, "step reward", reward)
        print("step reward", reward)
      total_reward += reward
      total_features = tuple(x + y for x,y in zip(total_features, features))
 
      if done:
        break

    if render_mode:
      print("reward", total_reward, "timesteps", t)
    reward_list.append(total_reward)
    t_list.append(t)

  total_features = tuple(x + y for x,y in zip(total_features, features))
  total_features = tuple(x/t for x in total_features)

  episodes_reward_sum += total_reward
  episodes_feature_sum = tuple(x + y for x,y in zip(episodes_feature_sum, total_features))

  episode_avg_reward = episodes_reward_sum / num_episode
  episode_avg_features = tuple(x/num_episode for x in episodes_feature_sum)

  #return reward_list, t_list
  #print("MODEL: REWARD", episode_avg_reward)
  #print("MODEL: AVG FEATURES (orig, leg0, leg1)", episode_avg_features)
  #return tuple(reward_list), tuple(total_features)

  #scores = {'AvgReward': episode_avg_reward, 'Distance', 

  scores = {
      "meanAvgReward": episode_avg_reward,
      "meanDistance": episode_avg_features[0],
      "meanHeadStability": episode_avg_features[1],
      "meanTorquePerStep": episode_avg_features[2],
      "meanJump": episode_avg_features[3],
      "meanLeg0HipAngle": episode_avg_features[4],
      "meanLeg0HipSpeed": episode_avg_features[5],
      "meanLeg0KneeAngle": episode_avg_features[6],
      "meanLeg0KneeSpeed": episode_avg_features[7],
      "meanLeg1HipAngle": episode_avg_features[8],
      "meanLeg1HipSpeed": episode_avg_features[9],
      "meanLeg1KneeAngle": episode_avg_features[10],
      "meanLeg1KneeSpeed": episode_avg_features[11]
  }

  #return (episode_avg_reward,), tuple(episode_avg_features)
  return scores


# MODELINE	"{{{1
# vim:expandtab:softtabstop=2:shiftwidth=2:fileencoding=utf-8
# vim:foldmethod=marker
