FROM lcazenille/ubuntupyssh:latest
MAINTAINER leo.cazenille@gmail.com

ENV DEBIAN_FRONTEND noninteractive

# Install some basic utilities
RUN apt-get update && apt-get install -y \
    curl \
    ca-certificates \
    sudo \
    git \
    bzip2 \
    libx11-6 \
    python3-yaml \
    gosu \
    rsync \
    python3-opengl \
    python3-dev \
    python3-pip \
    build-essential \
    cmake \
    swig \
 && rm -rf /var/lib/apt/lists/*


# Create ssh key
RUN \
    mkdir /home/user/.ssh && \
    ssh-keygen -q -t rsa -N '' -f /home/user/.ssh/id_rsa && \
    cat /home/user/.ssh/id_rsa.pub > /home/user/.ssh/authorized_keys

# Download repositories
RUN \
    git clone https://gitlab.com/leo.cazenille/noisy-qd.git /home/user/noisyqd && \
    rm -fr /home/user/noisyqd/.git*

RUN pip3 install gym==0.9.4 box2d-py PyOpenGL setproctitle pybullet qdpy[all] cma
RUN pip3 uninstall -y qdpy && pip3 install --upgrade --no-cache-dir -r /home/user/noisyqd/requirements.txt

RUN mkdir -p /home/user

# Prepare for entrypoint execution
#CMD ["bash"]
ENTRYPOINT ["/home/user/noisyqd/entrypoint.sh"]
#CMD ["1000", "normal"]

# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
